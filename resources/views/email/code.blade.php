<h1>Hello {{ $user->first_name }},</h1>

<p>Use the following code to validate your email when resetting your password in Mi-Route.</p>

<p>
    <strong>Code: {{$validation_code}}</strong>
</p>

<p>
    This code will expire in 20 minutes.
</p>

<p>
    Regards,<br>

    {{ config('mail.from')['name'] }}
</p>
