<?php

require_once realpath('.') . '/tests/TestHelper.php';

use App\User;
use App\DailyMileage;

class DrivingTest extends TestCase
{
    private $helper;

    public function __construct(?string $name = null, array $data = [], string $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $this->helper = new TestHelper();
    }

    /**
     * @group driving
     * @group get_resource
     */
    public function test_get_driving_resource()
    {
        $user = User::find(12137);
        $token = JWTAuth::fromUser($user);

        $object = $this->actingAs($user)->json('get', '/api/driving', [], ['Authorization' => 'Bearer ' . $token]);
        #echo $object->response->content();
        $this->seeStatusCode(200);
        $this->seeJsonStructure([
            'last_ending_odometer',
            'last_entry_date',
            'last_starting_odometer',
            'lastStop',
            'trips'
        ], $object->response->content());
    }

    /**
     * @group driving
     * @group submit_ending_current
     */
    public function test_submit_ending_odometer_current_day()
    {
        # todo: can't use execution time because it will not see LUMEN_START from index.php
        define("LUMEN_START", microtime(true));

        # todo: this depends on the database being up-to-date
        $date = date('Y-m-d');
        $dailyMileage = \App\DailyMileage::where('ending_odometer', null)
                ->whereNotNull('starting_odometer')->count()->where('date_stamp', $date)->whereRaw("user_id in (select user_id from all_green_drivers)")->orderBy('date_stamp', 'desc')->first();

        if (empty($dailyMileage)) {
            $this->assertEquals(1, 2);
        } else {

            $user = User::find($dailyMileage->user_id);
            $token = JWTAuth::fromUser($user);

            $object = $this->actingAs($user)->json('put', '/api/endingOdometer', ['ending_day' => $date, 'ending_odometer' => $dailyMileage->starting_odometer + 100], ['Authorization' => 'Bearer ' . $token]);

            $this->seeStatusCode(200);
            $this->seeJsonStructure(['miroute_page'], $object->response->content());

        }
    }

    /**
     * @group driving
     * @group ending_odo_previous
     */
    public function test_submit_ending_odometer_previous_day()
    {
        $date = date('Y-m-d');
        $dailyMileage = \App\DailyMileage::where('ending_odometer', null)->where('date_stamp', '<', $date)->whereRaw("user_id in (select user_id from all_green_drivers)")->orderBy('date_stamp', 'desc')->first();
        $user = User::find($dailyMileage->user_id);
        $token = JWTAuth::fromUser($user);

        $object = $this->actingAs($user)->json('put', '/api/endingOdometer', ['ending_day' => $dailyMileage->date_stamp, 'ending_odometer' => $dailyMileage->starting_odometer + 100], ['Authorization' => 'Bearer ' . $token]);
        // todo: this prints out the response, not continue with the test.
        $this->seeStatusCode(200);
        $this->seeJsonStructure(['miroute_page'], $object->response->getContent());
    }

    /**
     * @group driving
     * @group submit_odo
     */
    public function test_submit_starting_odometer()
    {
        $date = date('Y-m-d');

        $dailyMileage = DailyMileage::where('ending_odometer', '>', 0)
            ->where('date_stamp', '<', $date)
            ->whereRaw("user_id in (select user_id from all_green_drivers)")
            ->orderBy('date_stamp', 'desc')
            ->first();

        $user = User::find($dailyMileage->user_id);
        echo $user->user_id . "\n";
        $token = JWTAuth::fromUser($user);

        $sampleStop = \App\MiDriverSavedStop::where('user_id', $user->user_id)->first();

        $object = $this->actingAs($user)->json('post', '/api/startingOdometer', [
            'starting_odometer' => $dailyMileage->ending_odometer,
            'business_purpose' => 'testing',
            'stop_type' => 'driver',
            'stop_id' => $sampleStop->driver_stop_id
        ], ['Authorization' => 'Bearer ' . $token]);

        $this->seeStatusCode(200);
        $this->seeJsonStructure(['message'], $object->response->content());
    }
}
