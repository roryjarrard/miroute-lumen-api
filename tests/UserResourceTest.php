<?php

use App\User;

require_once realpath('.') . '/tests/TestHelper.php';

class UserResourceTest extends TestCase
{
    private $helper;

    public function __construct(?string $name = null, array $data = [], string $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $this->helper = new TestHelper();
    }

    /**
     * @group User
     * @group Password
     */
    public function test_validate_user_email_for_password_reset()
    {
        $user = User::inRandomOrder()->first();
        $object = $this->json('post', '/api/validateEmail', ['email' => $user->email]);

        $this->seeStatusCode(200);
        $this->seeJsonStructure(['message'], json_decode($object->response->content(), true));
    }

    /**
     * @group User
     * @group Password
     */
    public function test_validate_non_user_email_for_password_reset()
    {
        $object = $this->json('post', '/api/validateEmail', ['email' => 'xxx@yyy.zzz']);

        $this->seeStatusCode(400);
        $this->seeJsonStructure(['title', 'message', 'origin_message', 'origin_status'], json_decode($object->response->content(), true));
    }

    /**
     * @group User
     */
    public function testGetUserResources()
    {
        $user = factory(User::class)->create();
        $token = JWTAuth::fromUser($user);

        $object = $this->actingAs($user)->json('get', '/api/user', [], ['Authorization' => 'Bearer ' . $token]);

        #die(var_dump(json_decode($object->response->content(), true)));


        $this->seeStatusCode(200);


        $this->seeJsonStructure([
            'company_id',
            'company_name',
            'country',
            'email',
            'first_name',
            'is_demo_driver',
            'last_name',
            'mileage_entry_method',
            'mileage_label',
            'use_stop_contract',
            'use_tickets',
            'user_id',
            'username'
        ], json_decode($object->response->content(), true));

        $id = $user->user_id;
        $user->delete();
        \DB::statement("alter table ap_user AUTO_INCREMENT = {$id}");
    }
}
