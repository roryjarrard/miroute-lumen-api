<?php

use App\User;
use App\Driver;
use App\MiDriver;
use Illuminate\Support\Facades\DB;

class TestHelper
{
    public function createUser($options = [])
    {
        $user = factory(User::class)->create($options);
        $user->password = app('hash')->make('silver');
        $user->save();

        if ($user->type == 'driver') {
            $driver = factory(Driver::class)->create([
                'user_id' => $user->user_id,
                'vehicle_profile_id' => 1,
                'mileage_band_id' => 1
            ]);
        }

        return $user;
    }

    public function makeUser()
    {
        return factory(User::class)->make();
    }

    public function makeMiDriver($options = [])
    {
        if (sizeof($options)) {
            $midriver = MiDriver::make($options);
            $midriver->timestamps = false;
            $midriver->save();
        }
    }

    public function removeUser($user_id)
    {
        Driver::where('user_id', $user_id)->delete();
        MiDriver::where('user_id', $user_id)->delete();
        User::find($user_id)->delete();
        DB::statement("alter table ap_user AUTO_INCREMENT = {$user_id}");
    }
}