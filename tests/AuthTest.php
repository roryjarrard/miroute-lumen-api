<?php

require_once realpath('.') . '/tests/TestHelper.php';
use App\User;
use App\Driver;



class AuthTest extends TestCase
{
    private $helper;

    public function __construct(?string $name = null, array $data = [], string $dataName = '')
    {
        $this->helper = new TestHelper();
        parent::__construct($name, $data, $dataName);
    }

    /**
     * @group Auth
     */
    public function testRegisterUser()
    {
        $user = $this->helper->makeUser()->toArray();
        $user['password'] = 'silver';
        $user['password_confirmation'] = 'silver';

        $object = $this->json('post', '/api/register', $user);
        $content = $object->response->content();

        // see status code 201: created
        $this->seeStatusCode(201);
        $this->seeJsonStructure([
            'user', 'token'
        ], $content);

        $this->helper->removeUser(json_decode($content)->user->user_id);
    }

    /**
     * @group Auth
     */
    public function test_login_nonexistent_driver()
    {
        $user = $this->helper->createUser(['active'=>'inactive', 'isApproved' => 1]);

        $object = $this->json('post', '/api/login', [
            'username' => 'xxxxxxxx',
            'password' => 'silver'
        ]);
        $content = json_decode($object->response->content());

        $this->seeStatusCode(401);
        $this->seeJsonStructure(['title', 'message'], $content);

        $this->helper->removeUser($user->user_id);
    }

    /**
     * @group Auth
     */
    public function test_login_inactive_driver()
    {
        $user = $this->helper->createUser(['active'=>'inactive']);

        $object = $this->json('post', '/api/login', [
            'username' => $user->username,
            'password' => 'silver'
        ]);
        $content = json_decode($object->response->content());

        $this->seeStatusCode(401);
        $this->seeJsonStructure(['title', 'message'], $content);

        $this->helper->removeUser($user->user_id);
    }

    /**
     * @group Auth
     */
    public function test_login_not_a_driver()
    {
        $user = $this->helper->createUser(['type'=>'manager']);

        $object = $this->json('post', '/api/login', [
            'username' => $user->username,
            'password' => 'silver'
        ]);
        $content = json_decode($object->response->content());

        $this->seeStatusCode(401);
        $this->seeJsonStructure(['title', 'message'], $content);

        $this->helper->removeUser($user->user_id);
    }

    /**
     * @group Auth
     */
    public function test_login_yellow_light_status()
    {
        $user = $this->helper->createUser(['type'=>'driver', 'active'=>'active']);
        $user->driver->street_light_id = 1;
        $user->driver->save();

        $object = $this->json('post', '/api/login', [
            'username' => $user->username,
            'password' => 'silver'
        ]);
        $content = json_decode($object->response->content());
        #die(json_encode($content));

        $this->seeStatusCode(401);
        $this->seeJsonStructure(['title', 'message'], $content);

        $this->helper->removeUser($user->user_id);
    }

    /**
     * @group Auth
     */
    public function test_login_red_light_status()
    {
        $user = $this->helper->createUser(['type'=>'driver', 'active'=>'active']);
        $user->driver->update(['street_light_id'=>512]); //

        $object = $this->json('post', '/api/login', [
            'username' => $user->username,
            'password' => 'silver'
        ]);
        $content = json_decode($object->response->content());

        $this->seeStatusCode(401);
        $this->seeJsonStructure(['title', 'message'], $content);

        $this->helper->removeUser($user->user_id);
    }

    /**
     * @group Auth
     */
    public function test_login_driver_with_stop_date_set()
    {
        $user = $this->helper->createUser(['type'=>'driver', 'active'=>'active']);
        $user->driver->update(['street_light_id'=>2, 'stop_date'=>'2017-01-01']);

        $object = $this->json('post', '/api/login', [
            'username' => $user->username,
            'password' => 'silver'
        ]);
        $content = json_decode($object->response->content());
        #die(json_encode($content));

        $this->seeStatusCode(401);
        $this->seeJsonStructure(['title', 'message'], $content);

        $this->helper->removeUser($user->user_id);
    }

    /**
     * @group Auth
     */
    public function test_login_not_miroute_not_test_not_demo_driver()
    {
        $user = $this->helper->createUser(['type'=>'driver', 'active'=>'active', 'company_id'=>1]);
        $user->driver->update(['street_light_id'=>2]);


        $object = $this->json('post', '/api/login', [
            'username' => $user->username,
            'password' => 'silver'
        ]);
        $content = json_decode($object->response->content());
        #die(json_encode($content));

        $this->seeStatusCode(401);
        $this->seeJsonStructure(['title', 'message'], $content);

        $this->helper->removeUser($user->user_id);
    }

    /**
     * @group Auth
     */
    public function test_login_is_miroute_not_started_not_demo_driver()
    {
        $user = $this->helper->createUser(['type'=>'driver', 'active'=>'active', 'company_id'=>13]);
        $next_year = date('Y') + 1;
        $user->driver->update(['street_light_id'=>2, 'start_date'=>$next_year.'-01-01']); // date in the future

        $this->helper->makeMiDriver(['user_id'=>$user->user_id, 'mi_start_date'=>'2007-01-01', 'enabled' => 0]);

        $object = $this->json('post', '/api/login', [
            'username' => $user->username,
            'password' => 'silver'
        ]);
        $content = json_decode($object->response->content());

        $this->seeStatusCode(401);
        $this->seeJsonStructure(['title', 'message'], $content);

        $this->helper->removeUser($user->user_id);
    }

    /**
     * @group Auth
     */
    public function test_login_is_miroute_is_demo()
    {
        $user = $this->helper->createUser(['type'=>'driver', 'active'=>'active', 'company_id'=>13]);
        $next_year = date('Y') + 1;
        $user->driver->update(['street_light_id'=>2, 'start_date'=>$next_year.'-01-01']); // date in the future


        $object = $this->json('post', '/api/login', [
            'username' => $user->username,
            'password' => 'silver'
        ]);
        $content = json_decode($object->response->content());
        #die(json_encode($content));

        $this->seeStatusCode(200);
        $this->seeJsonStructure(['token'], $content);

        $this->helper->removeUser($user->user_id);
    }

    /**
     * @group Auth
     */
    public function test_login_is_not_miroute_is_test()
    {
        $user = $this->helper->createUser(['type'=>'driver', 'active'=>'active', 'company_id'=>1]);
        $user->driver->update(['street_light_id'=>2]);
        $this->helper->makeMiDriver(['user_id'=>$user->user_id, 'mi_start_date' => '2017-01-01', 'enabled'=>1]);


        $object = $this->json('post', '/api/login', [
            'username' => $user->username,
            'password' => 'silver'
        ]);
        $content = json_decode($object->response->content());
        #die(json_encode($content));

        $this->seeStatusCode(200);
        $this->seeJsonStructure(['token'], $content);

        $this->helper->removeUser($user->user_id);
    }

    /**
     * @group Auth
     */
    public function test_login_regular_miroute()
    {
        $user = $this->helper->createUser(['type'=>'driver', 'active'=>'active', 'company_id'=>13]);
        $user->driver->update(['street_light_id'=>2]);

        $object = $this->json('post', '/api/login', [
            'username' => $user->username,
            'password' => 'silver'
        ]);
        $content = json_decode($object->response->content());

        $this->seeStatusCode(200);
        $this->seeJsonStructure(['token'], $content);

        $this->helper->removeUser($user->user_id);
    }
}