<?php

require_once realpath('.') . '/tests/TestHelper.php';
use App\User;

class StatusTest extends TestCase
{
    private $helper;

    public function __construct(?string $name = null, array $data = [], string $dataName = '')
    {
        $this->helper = new TestHelper();
        parent::__construct($name, $data, $dataName);
    }

    /**
     * @group Status
     * @group xxx
     * @group driving
     */
    public function testGetDriverStatus()
    {
        #$user = $this->helper->createUser();
        $user = User::find(27220);
        $token = JWTAuth::fromUser($user);

        $object = $this->actingAs($user)->json('get', '/api/status', [], ['Authorization' => 'Bearer ' . $token]);
        #echo($object->response->content());
        $this->seeStatusCode(200);
        $this->seeJsonStructure([
            'street_light_id',
            'street_light_color',
            'driverStatusItems',
            'miroute_page',
        ], json_decode($object->response->content(), true));
    }

    /*
    public function testGetUserResources()
    {
        $user = factory(User::class)->create();
        $token = JWTAuth::fromUser($user);

        $object = $this->actingAs($user)->json('get', '/api/user', [], ['Authorization' => 'Bearer ' . $token]);

        #die(var_dump(json_decode($object->response->content(), true)));


        $this->seeStatusCode(200);


        $this->seeJsonStructure([
            'company_id',
            'company_name',
            'country',
            'email',
            'first_name',
            'is_demo_driver',
            'last_name',
            'mileage_entry_method',
            'mileage_label',
            'use_stop_contract',
            'use_tickets',
            'user_id',
            'username'
        ], json_decode($object->response->content(), true));

        $id = $user->user_id;
        $user->delete();
        \DB::statement("alter table ap_user AUTO_INCREMENT = {$id}");
    }
    */
}