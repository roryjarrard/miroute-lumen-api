<?php

$date = date('Y-m-d H:i:s');

$prod = new mysqli('cardataconsultants.com', 'cardev', 'car99Mi78#/#', 'cardata_ca');
$local = new mysqli('localhost', 'root', 'root', 'cardata_api');

# truncate table
$truncate = true;
if ($truncate) {
    $local->query('set foreign_key_checks=0');
    $local->query('truncate table mi_company_params');
    $local->query('set foreign_key_checks=0');
}

$query = <<<EOQ
insert into mi_company_params (company_params_id, company_id, param_name, param_value) 
values (?,?,?,?)
EOQ;

$stmt = $local->prepare($query);

$res = $prod->query("select * from MI_Company_Params");
echo "Found " . $res->num_rows . " company param records\n";

while ($row = $res->fetch_assoc()) {

    $stmt->bind_param('iiss', $row['company_params_id'], $row['company_id'], $row['param_name'], $row['param_value']);
    $stmt->execute();
    if ($local->error) {
        echo $query . "\n";
        die($local->error . "\n");
    }
}
