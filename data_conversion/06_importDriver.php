<?php

$table_name = 'ap_driver';
$date = date('Y-m-d H:i:s');

$prod = new mysqli('cardataconsultants.com', 'cardev', 'car99Mi78#/#', 'cardata_ca');
$local = new mysqli('localhost', 'root', 'root', 'cardata_api');

$query = <<<EOQ
insert into ap_driver (user_id, division_id, street_light_id, vehicle_profile_id, mileage_band_id,
                       employee_num, territory, territory_list, start_date, stop_date, 
                       insurance_date, drivers_license_date, cost_centre, declaration_date, checked_liability_person,
                       checked_liability_accident, checked_property_damage, checked_deductible, checked_old_car, checked_business_use,
                       payrol_number, checked_disabled, checked_on_leave, checked_terminated, checked_liability,
                       checked_collision, checked_comprehensive, checked_not_qualified, uploaded_licence_date, reference,
                       business_unit, car_police_acceptance_date, checked_medical, checked_motorist_person, checked_motorist_accident,
                       checked_additional_insured, JobTitle, remove, office_type) 
values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)
EOQ;
$stmt = $local->prepare($query);

# truncate table
$truncate = true;
if ($truncate) {
    $local->query('set foreign_key_checks=0');
    $local->query("truncate table {$table_name}");
    $local->query('set foreign_key_checks=0');
}

$prod_query = <<<EOQ
select * from AP_Driver
EOQ;
$res = $prod->query($prod_query);

echo "Found " . $res->num_rows . " driver records\n";

while ($row = $res->fetch_object()) {

    // check existing
    $userExists = $local->query("select user_id from ap_user where user_id = {$row->user_id}");
    if ($userExists->num_rows > 0) {
        $stmt->bind_param('iiiiisssssssssiiiiiisiiiiiiisissiiiisis',
            $row->user_id, $row->division_id, $row->street_light_id, $row->vehicle_profile_id, $row->mileage_band_id,
            $row->employee_num, $row->territory, $row->territory_list, $row->start_date, $row->stop_date,
            $row->insurance_date, $row->drivers_license_date, $row->cost_centre, $row->declaration_date, $row->checked_liability_person,
            $row->checked_liability_accident, $row->checked_property_damage, $row->checked_deductible, $row->checked_old_car, $row->checked_business_use,
            $row->payrol_number, $row->checked_disabled, $row->checked_on_leave, $row->checked_terminated, $row->checked_liability,
            $row->checked_collision, $row->checked_comprehensive, $row->checked_not_qualified, $row->uploaded_licence_date, $row->reference,
            $row->business_unit, $row->car_police_acceptance_date, $row->checked_medical, $row->checked_motorist_person, $row->checked_motorist_accident,
            $row->checked_additional_insured, $row->JobTitle, $row->remove, $row->office_type
            );
        $stmt->execute();
        if ($local->error) {
            echo var_dump($row) . "\n";
            die($local->error . "\n");
        }
    }
}
