<?php
global $rewrite, $cdprod;

function titleCase($string)
{
    $word_splitters = array(' ', '-', "O'", "L'", "D'", 'St.', 'Mc');
    $lowercase_exceptions = array('the', 'van', 'den', 'von', 'und', 'der', 'de', 'da', 'of', 'and', "l'", "d'");
    $uppercase_exceptions = array('III', 'IV', 'VI', 'VII', 'VIII', 'IX');

    $string = strtolower($string);
    foreach ($word_splitters as $delimiter) {
        $words = explode($delimiter, $string);
        $newwords = array();
        foreach ($words as $word) {
            if (in_array(strtoupper($word), $uppercase_exceptions))
                $word = strtoupper($word);
            else
                if (!in_array($word, $lowercase_exceptions))
                    $word = ucfirst($word);

            $newwords[] = $word;
        }

        if (in_array(strtolower($delimiter), $lowercase_exceptions))
            $delimiter = strtolower($delimiter);

        $string = join($delimiter, $newwords);
    }
    return $string;
}

// Timing function
// Takes starting time as input and returns a string stating how much time has passed
function getTimeRunning($start)
{
    $elapsed = time() - $start;
    $minutes = (int)($elapsed / 60);
    $hours = (int)($minutes / 60);

    if ($elapsed < 60) {
        $totaltime = $elapsed . ' seconds';
    } elseif ($elapsed < 3600) {
        $totaltime = $minutes . ' min ' . ($elapsed % 60) . ' seconds';
    } else {
        $totaltime = $hours . ' hours ' . ($minutes % 60) . ' min ' . ($elapsed % 60) . ' seconds';
    }
    return $totaltime;
}


function getCompaniesData()
{
    global $rewrite;
    if (empty($rewrite)) {
        die("\nRewrite DB connection failure in helpers.php\n");
    }

    $companies_array = [];
    $companies_status = []; // true = active, false = inactive


    $res = $rewrite->query("select id, deleted_at from companies order by id");
    if ($rewrite->error) {
        die("REWRITE ERROR:\nCould not query companies in helpers.php: {$rewrite->error}\n");
    }

    while ($row = $res->fetch_assoc()) {
        $companies_array[] = $row['id'];
        $companies_status[$row['id']] = empty($row['deleted_at']) ? true : false;
    }

    $companies_list = implode(',', $companies_array);

    return [
        'companies_array' => $companies_array,
        'companies_status' => $companies_status,
        'companies_list' => $companies_list
    ];
}

function getDivisionsData()
{
    global $rewrite;

    $divisions_array = [];
    $divisions_status = []; // true = active, false = inactive

    $res = $rewrite->query("select id, deleted_at from divisions order by id");
    while ($row = $res->fetch_assoc()) {
        $divisions_array[] = $row['id'];
        $divisions_status[$row['id']] = empty($row['deleted_at']) ? true : false;
    }

    $divisions_list = implode(',', $divisions_array);

    return [
        'divisions_array' => $divisions_array,
        'divisions_status' => $divisions_status,
        'divisions_list' => $divisions_list
    ];
}

/**
 * After data is converted, we transfer the data from the transition database to the rewrite database
 *
 * @param array $tables
 */
function transferDataToRewrite($tables = [])
{
    global $transition_user, $transition_password, $transition_database, $rewrite_user, $rewrite_password, $rewrite_database;
    if (count($tables))
    {
        $tableNames = '';
        foreach ($tables as $table) {
            $tableNames .= $table . ' ';
        }
        $cmd = <<<EOQ
mysqldump -u{$transition_user} -p{$transition_password} {$transition_database} --tables {$tableNames} | mysql -u{$rewrite_user} -p{$rewrite_password} {$rewrite_database}
EOQ;
        echo "Transferring tables {$tableNames} data to the database {$rewrite_database}...\n";
        exec($cmd);
    }
}

function executePreparedStatement($db, $stmt, $types, $params)
{
    $res = [
        'insert_id' => -1,
        'error' => null
    ];
//    $len = strlen($types);
//    echo "types: {$len} $types\n";
//    echo var_dump($params);
//    die();

    $stmt->bind_param($types, ...$params);
    if ($db->error) {
        $res['error'] = $db->error;
        return $res;
    }

    $stmt->execute();
    if ($db->error) {
        $res['error'] = $db->error;
        return $res;
    }

    $res['insert_id'] = $db->insert_id;
    return $res;
}


function getTableCount(mysqli $database, $query)
{
    $req = $database->query($query);
    if ($database->error) {
        echo "Error getting count:\n{$query}\n";
    }

    if (empty($req) || $req->num_rows == 0 || $database->error) {
        return 0;
    } else {
        return $req->fetch_assoc()['total'];
    }
}