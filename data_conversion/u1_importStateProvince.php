<?php

$prod = new mysqli('cardataconsultants.com', 'cardev', 'car99Mi78#/#', 'cardata_ca');
$local = new mysqli('localhost', 'root', 'root', 'cardata_api');

$local->query("truncate table state_province");

$query = "select * from State_Province";
$res = $prod->query($query);
echo "Number of rows: " . $res->num_rows . "\n";
while ($row = $res->fetch_assoc()) {
    //die(var_dump($row));

    $state_exists = $local->query("select count(*) as `total` from state_province where state_province_id={$row['state_province_id']} and country='{$row['country']}'")->fetch_assoc()['total'];
    if ($state_exists<1) {
        $local->query(<<<EOQ
insert into state_province (state_province_id, name, abbreviation, country) 
values ({$row['state_province_id']}, '{$row['name']}', '{$row['abbreviation']}', '{$row['country']}')
EOQ
        );
        if ($local->error) {
            die($local->error . "\n");
        }
    }
}

