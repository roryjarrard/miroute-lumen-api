<?php

$table_name = 'ap_admin';
$date = date('Y-m-d H:i:s');

$prod = new mysqli('cardataconsultants.com', 'cardev', 'car99Mi78#/#', 'cardata_ca');
$local = new mysqli('localhost', 'root', 'root', 'cardata_api');

# truncate table
$truncate = true;
if ($truncate) {
    $local->query('set foreign_key_checks=0');
    $local->query("truncate table {$table_name}");
    $local->query('set foreign_key_checks=0');
}

// prepare statement
$q = <<<EOQ
insert into {$table_name} (user_id, company_admin, company_approver, IsLimitedAdmin)
values (?,?,?,?)
EOQ;

$stmt = $local->prepare($q);
if ($local->error) {
    die("Error in prepare: " . $local->error . "\n\n");
}

$res = $prod->query("select * from AP_Admin");
echo "{$table_name}: {$res->num_rows} rows\n";
while ($row = $res->fetch_object()) {
    $limited = !isset($row->IsLimitedAdmin) ? 0 : $row->IsLimitedAdmin;
    $stmt->bind_param('iiii', $row->user_id, $row->company_admin, $row->company_approver, $limited);
    $stmt->execute();
    if ($local->error) {
        echo "Error in insert: {$local->error}\n\n";
    }
}


