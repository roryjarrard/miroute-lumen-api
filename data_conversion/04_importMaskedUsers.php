<?php
set_include_path(get_include_path() . ';' . realpath('..'));
require_once('vendor/fzaninotto/faker/src/autoload.php');

$faker = Faker\Factory::create();
$table_name = 'ap_user';
$date = date('Y-m-d H:i:s');
$null = NULL;

$prod = new mysqli('cardataconsultants.com', 'cardev', 'car99Mi78#/#', 'cardata_ca');
$local = new mysqli('localhost', 'root', 'root', 'cardata_api');

# truncate table
$truncate = true;
if ($truncate) {
    $local->query('set foreign_key_checks=0');
    $local->query("truncate table {$table_name}");
    $local->query('set foreign_key_checks=0');
}

// prepare statement
$q = <<<EOQ
insert into ap_user (user_id, company_id, username, `password`, first_name, last_name, email, active, `type`, phone, isApproved, created_on) 
values (?,?,?,?,?,?,?,?,?,?,?,?)
EOQ;

$stmt = $local->prepare($q);
if ($local->error) {
    die("Error in prepare: " . $local->error . "\n\n");
}

$res = $prod->query("select * from AP_User");
echo "Found " . $res->num_rows . " users\n";

while ($row = $res->fetch_object()) {
    $firstName = preg_replace('/[^A-Za-z0-9\-]/', '', $faker->firstName);
    $lastName = preg_replace('/[^A-Za-z0-9\-]/', '', $faker->lastName);
    $username = generateUsername($local, $firstName, $lastName);
    $password = password_hash('silver', PASSWORD_BCRYPT);
    $email = $faker->email;

    $date = empty($row->created_on) ? date('Y-m-d H:i:s') : $row->created_on;

    $stmt->bind_param('iissssssssis', $row->user_id, $row->company_id, $username, $password, $firstName, $lastName, $email,
        $row->active, $row->type, $row->phone, $row->isApproved, $date);
    $stmt->execute();
    if ($local->error) {
        echo "Error in insert: {$local->error}\n\n";
    }
}

function generateUsername(mysqli $conn, $firstName, $lastName)
{
    $username = strtolower(substr($firstName, 0, 1) . substr($lastName, 0, 10));
    //echo "Testing $username, count: ";

    $username_taken = $conn->query(<<<EOQ
select count(*) as `total` from ap_user where username = '{$username}'
EOQ
    )->fetch_assoc();
    //echo $username_taken['total'] . "\n";
    $count = 2;
    $width = 1;
    while (empty($username_taken) || $username_taken['total'] > 0) {
        $username = strtolower(substr($firstName, 0, 1) . $count . substr($lastName, 0, 10 - $width));
        //echo "Testing $username, count: ";

        $username_taken = $conn->query(<<<EOQ
select count(*) as `total` from ap_user where username = '{$username}'
EOQ
        )->fetch_assoc();
        //echo $username_taken['total'] . "\n";
        $count++;
        if ($count > 9)
            $width = 2;
    }

    return $username;
}
