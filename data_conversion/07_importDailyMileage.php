<?php

$date = date('Y-m-d H:i:s');

$prod = new mysqli('cardataconsultants.com', 'cardev', 'car99Mi78#/#', 'cardata_ca');
$local = new mysqli('localhost', 'root', 'root', 'cardata_api');

# truncate table
$truncate = true;
if ($truncate) {
    $local->query('set foreign_key_checks=0');
    $local->query('truncate table ap_daily_mileage');
    $local->query('set foreign_key_checks=0');
}

$query = <<<EOQ
insert into ap_daily_mileage (user_id, date_stamp, destination, business_purpose, 
                              starting_odometer, ending_odometer, business_mileage, commuter_mileage) 
values (?,?,?,?,?,?,?,?)
EOQ;
$stmt = $local->prepare($query);

for ($year = 2018; $year < 2020; $year++) {
    $nextYear = $year + 1;

    for ($month = 1; $month <= 12; $month++) {
        $startDate = sprintf('%04d-%02d-%02d', $year, $month, 1);
        $endDate = $month < 12 ? sprintf('%04d-%02d-%02d', $year, $month + 1, 1) : sprintf('%04d-%02d-%02d', $nextYear, 1, 1);

        $res = $prod->query("select * from AP_Daily_Mileage where date_stamp >= '$startDate' and date_stamp < '$endDate' order by date_stamp, user_id");
        echo $startDate . ": " . $res->num_rows . " records\n";

        while ($row = $res->fetch_object()) {
            // check existing
            $userExists = $local->query("select user_id from ap_user where user_id = {$row->user_id}");
            if ($userExists->num_rows > 0) {
                $stmt->bind_param('isssiiii', $row->user_id, $row->date_stamp, $row->destination, $row->business_purpose, $row->starting_odometer,
                    $row->ending_odometer, $row->business_mileage, $row->commuter_mileage);
                $stmt->execute();
                if ($local->error) {
                    echo $local->error . "\n\n";
                    echo 'QUERY: ' . "\n" . $query . "\n\n";
                    echo var_dump($row) . "\n";
                    continue;
                }
            }
        }
    }
}
