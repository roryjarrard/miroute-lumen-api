<?php
set_include_path(get_include_path() . ';' . realpath('..'));
require_once('vendor/fzaninotto/faker/src/autoload.php');

$faker = Faker\Factory::create();
$table_name = 'ap_division';
$date = date('Y-m-d H:i:s');
$null = NULL;

$prod = new mysqli('cardataconsultants.com', 'cardev', 'car99Mi78#/#', 'cardata_ca');
$local = new mysqli('localhost', 'root', 'root', 'cardata_api');

# truncate table
$truncate = true;
if ($truncate) {
    $local->query('set foreign_key_checks=0');
    $local->query("truncate table {$table_name}");
    $local->query('set foreign_key_checks=0');
}

// prepare statement
$q = <<<EOQ
insert into ap_division (division_id, company_id, name) 
values (?,?,?)
EOQ;

$stmt = $local->prepare($q);
if ($local->error) {
    die("Error in prepare: " . $local->error . "\n\n");
}

$res = $prod->query("select * from AP_Division");
echo "Found " . $res->num_rows . " divisions\n";

$names = ['Sales', 'Marketing', 'Administration', 'Research', 'Human Resources', 'Accounting'];

while ($row = $res->fetch_object()) {
    $divisionName = $faker->randomElement($names);

    $stmt->bind_param('iis', $row->division_id, $row->company_id, $divisionName);
    $stmt->execute();

    if ($local->error) {
        echo "Error in insert: {$local->error}\n\n";
    }
}
