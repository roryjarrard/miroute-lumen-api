<?php
set_include_path(get_include_path() . ';' . realpath('..'));
require_once('vendor/fzaninotto/faker/src/autoload.php');

$faker = Faker\Factory::create();
$table_name = 'ap_company';
$date = date('Y-m-d H:i:s');

$prod = new mysqli('cardataconsultants.com', 'cardev', 'car99Mi78#/#', 'cardata_ca');
$local = new mysqli('localhost', 'root', 'root', 'cardata_api');

# truncate table
$truncate = true;
if ($truncate) {
    $local->query('set foreign_key_checks=0');
    $local->query("truncate table {$table_name}");
    $local->query('set foreign_key_checks=0');
}

// prepare statement
$q = <<<EOQ
insert into ap_company (company_id, `name`, mileage_reminder, mileage_lock_driver, 
                        mileage_lock_all, mileage_entry, direct_pay_date, direct_pay_file_fee,
                        direct_pay_driver_fee, approval_end_date, car_policy_pdf, car_policy_enforce,
                        insurance_person, insurance_accident, insurance_property, insurance_deductible,
                        insurance_plpd, insurance_collision, insurance_comprehensive, insurance_enforce,
                        insurance_fax, company_admin_user_id, company_admin_phone, driver_schedule,
                        start_stop_dates, insurance_dates, drivers_license_dates, driver_country,
                        capital_cost_adj, fuel_economy_adj, maintenance_adj, repair_adj,
                        depreciation_adj, insurance_adj, fuel_price_adj, resale_value_adj,
                        capital_cost_tax_adj, monthly_payment_adj, finance_cost_adj, fee_renewal_adj,
                        fixed_adj, variable_adj, status_id, demo_company,
                        notify_about_insurance_expiry, approved_by_user_id, approved_date, quarterly_tax_adjustment,
                        insurance_responsibility, license_enforce, locked_date, locked_user_id,
                        locked_reason, locked_type, licence_responsibility, licence_reminder,
                        licence_fax, draft_rates, serv_carDATA, serv_plan,
                        serv_management, serv_driver_calls, serv_driver_changes, serv_account_maintenance,
                        serv_mileage_inspection, serv_traffic_light_status, serv_mileage_recording, serv_vehicle_resp,
                        serv_dl_documentation, serv_mileage_band_audit, serv_insurance_documentation, serv_payment_responsibility,
                        insurance_grace_period, licence_grace_period, cardata_admin_user_id, cardata_admin_phone,
                        mileage_grace_period, insurance_grace_days, insurance_medical, motorist_person,
                        motorist_accident, fix_payment_advance, mileage_band_audit, remove_fixed_without_insurance,
                        remove_fixed_start_date, remove_fixed_end_date, IsDriverProfileEditable, IsShowManagerId,
                        IsShowTaxLimit, IsShowQuarterlyTaxAudit, IsUsingJobTitle, PayFileDate, 
                        AdvancedPayment, licence_renewal_dd, licence_renewal_mm, licence_transition_date,
                        insurance_processor_id, licence_processor_id, insurance_phone, licence_phone,
                        mileage_reminder_2, remove_reimbursement_without_insurance, restricted_country_access, favr_insurance_renewal,
                        invoice_frequency, fee_per_driver, no_reimbursement_reminder, hide_pdf_from_managers,
                        miroute_admin_user_id, miroute_admin_phone, use_stop_contract, no_odometer, use_commuter_mileage, use_commuter_mileage_transition_date)
values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,
        ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,
        ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,
        ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,
        ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,
        ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,
        ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,
        ?,?)
EOQ;

$stmt = $local->prepare($q);
if ($local->error) {
    die("Error in prepare: " . $local->error . "\n\n");
}

$res = $prod->query("select * from AP_Company");
echo "Found " . $res->num_rows . " companies\n";

while ($row = $res->fetch_object()) {
    $miroutePhone = $row->miroute_admin_phone == null || $row->miroute_admin_phone == '' ? '' : $row->miroute_admin_phone;
    $cardataPhone = $row->cardata_admin_phone == null || $row->cardata_admin_phone == '' ? '' : $row->cardata_admin_phone;
    $companyPhone = $row->company_admin_phone == null || $row->company_admin_phone == '' ? '' : $row->company_admin_phone;

    $mirouteAdmin = empty($row->miroute_admin_user_id) ? "NULL" : $row->miroute_admin_user_id;
    $cardataAdmin = empty($row->cardata_admin_user_id) ? "NULL" : $row->cardata_admin_user_id;
    $companyAdmin = empty($row->company_admin_user_id) ? "NULL" : $row->company_admin_user_id;

    $igp = explode(' ', $row->insurance_grace_period)[0];
    $insuranceGracePeriod = $igp = '0000-00-00' || empty($igp) ? NULL : $igp;

    $rfsd = explode(' ', $row->remove_fixed_start_date)[0];
    $removeFixedStartDate = $rfsd = '0000-00-00' || empty($rfsd) ? NULL : $rfsd;
    $rfed = explode(' ', $row->remove_fixed_start_date)[0];
    $removeFixedEndDate = $rfed = '0000-00-00' || empty($rfed) ? NULL : $rfed;

    $licence_transition_date = empty($row->licence_transition_date) || $row->licence_transition_date = '0000-00-00' ? NULL : $row->licence_transition_date;

    $licence_responsibility = trim($row->licence_responsibility) == '' ? NULL : trim($row->licence_responsibility);
    $insurance_responsibility = trim($row->insurance_responsibility) == '' ? NULL : trim($row->insurance_responsibility);



    $companyName = addslashes($faker->company);
    while (strlen($companyName) > 31)
        $companyName = addslashes($faker->company);

    $stmt->bind_param('isiiisiddiisiiiiiiississiiisiiiiiiiiiiiiddiiiisisssisssisiiiiiiiiiiiiiiissisiiiiiisissiiiiiiiiisiissiisssdiiisiiis',
        $row->company_id, $companyName, $row->mileage_reminder, $row->mileage_lock_driver,
        $row->mileage_lock_all, $row->mileage_entry, $row->direct_pay_date, $row->direct_pay_file_fee,
        $row->direct_pay_driver_fee, $row->approval_end_date, $row->car_policy_pdf, $row->car_policy_enforce,
        $row->insurance_person, $row->insurance_accident, $row->insurance_property, $row->insurance_deductible,
        $row->insurance_plpd, $row->insurance_collision, $row->insurance_comprehensive, $row->insurance_enforce,
        $row->insurance_fax, $companyAdmin, $companyPhone, $row->driver_schedule,
        $row->start_stop_dates, $row->insurance_dates, $row->drivers_license_dates, $row->driver_country,
        $row->capital_cost_adj, $row->fuel_economy_adj, $row->maintenance_adj, $row->repair_adj,
        $row->depreciation_adj, $row->insurance_adj, $row->fuel_price_adj, $row->resale_value_adj,
        $row->capital_cost_tax_adj, $row->monthly_payment_adj, $row->finance_cost_adj, $row->fee_renewal_adj,
        $row->fixed_adj, $row->variable_adj, $row->status_id, $row->demo_company,
        $row->notify_about_insurance_expiry, $row->approved_by_user_id, $row->approved_date, $row->quarterly_tax_adjustment,
        $insurance_responsibility, $row->license_enforce, $row->locked_date, $row->locked_user_id,
        $row->locked_reason, $row->locked_type, $licence_responsibility, $row->licence_reminder,
        $row->licence_fax, $row->draft_rates, $row->serv_carDATA, $row->serv_plan,
        $row->serv_management, $row->serv_driver_calls, $row->serv_driver_changes, $row->serv_account_maintenance,
        $row->serv_mileage_inspection, $row->serv_traffic_light_status, $row->serv_mileage_recording, $row->serv_vehicle_resp,
        $row->serv_dl_documentation, $row->serv_mileage_band_audit, $row->serv_insurance_documentation, $row->serv_payment_responsibility,
        $insuranceGracePeriod, $row->licence_grace_period, $cardataAdmin, $cardataPhone,
        $row->mileage_grace_period, $row->insurance_grace_days, $row->insurance_medical, $row->motorist_person,
        $row->motorist_accident, $row->fix_payment_advance, $row->mileage_band_audit, $row->remove_fixed_without_insurance,
        $removeFixedStartDate, $removeFixedEndDate, $row->IsDriverProfileEditable, $row->IsShowManagerId,
        $row->IsShowTaxLimit, $row->IsShowQuarterlyTaxAudit, $row->IsUsingJobTitle, $row->PayFileDate,
        $row->AdvancedPayment, $row->licence_renewal_dd, $row->licence_renewal_mm, $licence_transition_date,
        $row->insurance_processor_id, $row->licence_processor_id, $row->insurance_phone, $row->licence_phone,
        $row->mileage_reminder_2, $row->remove_reimbursement_without_insurance, $row->restricted_country_access, $row->favr_insurance_renewal,
        $row->invoice_frequency, $row->fee_per_driver, $row->no_reimbursement_reminder, $row->hide_pdf_from_managers,
        $mirouteAdmin, $miroutePhone, $row->use_stop_contract, $row->no_odometer, $row->use_commuter_mileage, $row->use_commuter_mileage_transition_date);

    $stmt->execute();
    if ($local->error) {
        echo "Error in insert: {$local->error}\n$insurance_responsibility\n";
    }
}
