<?php

$date = date('Y-m-d H:i:s');

$prod = new mysqli('localhost', 'root', 'root', 'cardata_ca');
$local = new mysqli('localhost', 'root', 'root', 'cardata_api');

# truncate table
$truncate = true;
if ($truncate) {
    $local->query('set foreign_key_checks=0');
    $local->query('truncate table ap_personal_mileage');
    $local->query('set foreign_key_checks=0');
}

$query = <<<EOQ
insert into ap_personal_mileage (user_id, entry_date, personal_1, personal_2, vehicle_changed) 
values (?,?,?,?,?)
EOQ;
$stmt = $local->prepare($query);

for ($year = 2018; $year < 2020; $year++) {
    for ($month = 1; $month <= 12; $month++) {
        echo "Starting $year month $month\n";
        for ($day = 1; $day <= 31; $day) {
            $entryDate = sprintf('%04d-%02d-%02d', $year, $month, $day);

            $res = $prod->query("select * from ap_personal_mileage where entry_date = '{$entryDate}'order by user_id");

            while ($row = $res->fetch_assoc()) {
                // check existing
                $userExists = $local->query("select user_id from ap_user where user_id = {$row['user_id']}");
                if ($userExists->num_rows > 0) {
                    $stmt->bind_param('isiii', $row['user_id'], $entryDate, $row['personal_1'], $row['personal_2'], $row['vehicle_changed']);
                    $stmt->execute();
                    if ($local->error) {
                        echo $query . "\n";
                        die($local->error . "\n");
                    }
                }
            }
        }
    }
}