# Mi-Route API

This API is intended to communicate with the Mi-Route app (mobile or web) and the CarData backend database.

## Modifications from Laravel

Since Lumen is lightweight, there are some actions requiring manual intervention.

- Provider and Facade registration goes in bootstrap/app.php
- config folder and config/auth.php had to be duplicated from vendor\laravel\lumen-framework\config folder 
    - guards had to be modified to use JWT
- App/Providers/AuthServiceProvider::boot had to be modified (see commented out and new code)

## CarData Modifications

Since we are held to current schema, some modifications had to be made to have Laravel/Lumen models talk to our database

- User model
    - Custom table name (users => ap_user)
    - Custom primary key (id => user_id) 
