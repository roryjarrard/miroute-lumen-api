<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {

    return env('APP_ENV') . "<br>" . $router->app->version();
});
$router->post('/', function (Request $request) use ($router) {
    return ['hello' => 'rory'];
});
$router->get('/test', function () use ($router) {
    return "TEST OK";
});

/**
 * These are unprotected api routes
 */
$router->group(['prefix' => 'api'], function () use ($router) {
    $router->post('register', 'UserController@register');
    $router->post('login', 'AuthController@login');
    $router->get('open', 'DataController@open');
    $router->post('validateEmail', 'UserController@validateEmail');
    $router->post('validateCode', 'UserController@validateCode');
    $router->post('submitNewPassword', 'UserController@submitNewPassword');
});

/**
 * These are protected api routes
 */
$router->group(['middleware' => ['jwt.verify'], 'prefix' => 'api'], function () use ($router) {
    $router->get('logout', 'AuthController@logout');
    $router->get('refresh', 'AuthController@refreshToken');
    $router->get('closed', 'DataController@closed');
    $router->put('endingOdometer', 'DrivingController@submitEndingOdometer');
    $router->get('driving', 'DrivingController@getDrivingResource');
    $router->get('stops', 'DrivingController@getNearbyStops');
    $router->post('startingOdometer', 'DrivingController@submitStartingOdometer');
    $router->post('stop', 'DrivingController@submitNewStop');
    $router->put('stop', 'DrivingController@submitStop');
    $router->put('trip', 'DrivingController@updateTripMileage');
    $router->patch('status', 'StatusController@updateCurrentPage');
    $router->get('coordinator', 'UserController@getCoordinatorResource');
    $router->get('user', 'UserController@getUserResource');
    $router->get('getOfficeType', 'UserController@getOfficeType');
    $router->get('getCommuteMileage', 'DrivingController@getCommuteMileage');
});

/**
 * These are unprotected routes to test functionality (DANGER, DANGER...)
 */
$router->get('/testUser/{user_id}', function ($user_id) {
    return App\User::find($user_id)->company;
    #return App\User::find($user_id)->miDriver;
});

$router->get('/testCompany/{company_id}', function ($company_id) {
    return App\Company::find($company_id)->mirouteParams()->where('param_name', 'transition date')->first();
});

$router->get('/testCoord/{company_id}', function ($company_id) {
    $coord = App\Providers\CoordinatorServiceProvider::getCoordinator($company_id, 'cardata');
    return $coord->get();
});

