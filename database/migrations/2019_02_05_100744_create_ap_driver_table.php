<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApDriverTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ap_driver', function (Blueprint $table) {
            $table->integer('user_id')->unsigned();
            $table->smallInteger('division_id')->unsigned();
            $table->integer('street_light_id')->unsigned();
            $table->smallInteger('vehicle_profile_id')->unsigned();

            $table->tinyInteger('mileage_band_id')->unsigned();
            $table->string('employee_num', 16);
            $table->enum('territory', ['home_city', 'home_state', 'multicity', 'multistate']);
            $table->string('territory_list', 255);

            $table->date('start_date')->nullable();
            $table->date('stop_date')->nullable();
            $table->date('insurance_date')->nullable();
            $table->date('drivers_license_date')->nullable();

            $table->string('cost_centre', 45)->nullable();
            $table->date('declaration_date')->nullable();
            $table->boolean('checked_liability_person')->default(false);
            $table->boolean('checked_liability_accident')->default(false);

            $table->boolean('checked_property_damage')->default(false);
            $table->boolean('checked_deductible')->default(false);
            $table->boolean('checked_old_car')->default(false);
            $table->boolean('checked_business_use')->default(false);

            $table->string('payrol_number', 45)->nullable()->comment('spelling original');
            $table->boolean('checked_disabled')->default(false);
            $table->boolean('checked_on_leave')->default(false);
            $table->boolean('checked_terminated')->default(false);

            $table->boolean('checked_liability')->default(false);
            $table->boolean('checked_collision')->default(false);
            $table->boolean('checked_comprehensive')->default(false);
            $table->boolean('checked_not_qualified')->default(false);

            $table->dateTime('uploaded_licence_date')->nullable()->comment('spelling original');
            $table->integer('reference')->unsigned()->nullable()->comment('using when sending email  to get mileage ');
            $table->string('business_unit', 45)->nullable()->comment('WM specific');
            $table->dateTime('car_police_acceptance_date')->nullable()->comment('spelling original');

            $table->boolean('checked_medical')->default(false);
            $table->boolean('checked_motorist_person')->default(false);
            $table->boolean('checked_motorist_accident')->default(false);
            $table->boolean('checked_additional_insured')->default(false);

            $table->string('JobTitle', 255)->nullable();
            $table->boolean('remove')->default(false);
            $table->enum('office_type', ['In-Office', 'Remote'])->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ap_driver');
    }
}
