<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMiDriverTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mi_driver', function (Blueprint $table) {
            $table->integer('user_id')->unsigned();
            $table->boolean('enabled')->nullable()->comment('1 to enable this user, 0 to disable, null (or no row) to use company setting');
            $table->date('mi_start_date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mi_driver');
    }
}
