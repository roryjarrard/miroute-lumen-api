<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApDailyMileageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ap_daily_mileage', function (Blueprint $table) {
            $table->integer('user_id')->unsigned();
            $table->date('date_stamp');
            $table->string('destination', 500)->nullable();
            $table->string('business_purpose', 500)->nullable();
            $table->mediumInteger('starting_odometer')->unsigned()->nullable();
            $table->mediumInteger('ending_odometer')->unsigned()->nullable();
            $table->smallInteger('business_mileage')->unsigned()->nullable();
            $table->smallInteger('commuter_mileage')->unsigned()->nullable();
            $table->unique(['user_id', 'date_stamp'], 'user_date_unique');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ap_daily_mileage');
    }
}
