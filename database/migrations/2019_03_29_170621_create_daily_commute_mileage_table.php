<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDailyCommuteMileageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('daily_commute_mileage', function (Blueprint $table) {
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id', 'user_id_fk')->references('user_id')->on('ap_user');

            $table->date('date');
            $table->decimal('starting_commute', 5, 1)->default(0);
            $table->decimal('ending_commute', 5, 1)->default(0);
            $table->unique(['user_id', 'date'], 'user_id_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('daily_commute_mileage', function (Blueprint $table) {
            $table->dropForeign('user_id_fk');
        });
        Schema::dropIfExists('daily_commute_mileage');
    }
}
