<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApPersonalMileageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ap_personal_mileage', function (Blueprint $table) {
            $table->integer('user_id')->unsigned();
            $table->date('entry_date');
            $table->integer('personal_1');
            $table->integer('personal_2');
            $table->boolean('vehicle_changed');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ap_personal_mileage');
    }
}
