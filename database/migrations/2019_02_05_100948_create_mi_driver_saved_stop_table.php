<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMiDriverSavedStopTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mi_driver_saved_stop', function (Blueprint $table) {
            $table->increments('driver_stop_id');
            $table->integer('user_id')->unsigned();
            $table->string('name', 80);
            $table->string('address', 200);
            $table->string('city', 80)->nullable();
            $table->string('state_province', 2)->nullable();
            $table->string('zip_code', 10)->nullable();
            $table->string('country', 2)->nullable();
            $table->double('latitude');
            $table->double('longitude');
            $table->boolean('status')->default(true);
            $table->dateTime('time_created')->nullable()->comment('date and time created');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mi_driver_saved_stop');
    }
}
