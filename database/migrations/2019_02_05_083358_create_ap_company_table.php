<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApCompanyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ap_company', function (Blueprint $table) {
            $table->increments('company_id');

            $table->string('name', 32);
            $table->tinyInteger('mileage_reminder')->unsigned()->nullable();
            $table->tinyInteger('mileage_lock_driver')->unsigned();

            $table->tinyInteger('mileage_lock_all')->unsigned();
            $table->enum('mileage_entry', ['monthly', 'monthly personal', 'daily log', 'weekly log', 'daily commuter', 'miroute']);
            $table->tinyInteger('direct_pay_date')->unsigned()->nullable();
            $table->decimal('direct_pay_file_fee', 6, 2)->unsigned()->nullable();

            $table->decimal('direct_pay_driver_fee', 5, 2)->unsigned()->nullable();
            $table->tinyInteger('approval_end_date')->unsigned()->nullable();
            $table->boolean('car_policy_pdf')->default(false);
            $table->enum('car_policy_enforce', ['red light', ''])->nullable();

            $table->mediumInteger('insurance_person')->unsigned()->nullable()->default(100000);
            $table->mediumInteger('insurance_accident')->unsigned()->nullable()->default(300000);
            $table->mediumInteger('insurance_property')->unsigned()->nullable()->default(50000);
            $table->smallInteger('insurance_deductible')->unsigned()->nullable()->default(500);

            $table->mediumInteger('insurance_plpd')->unsigned()->default(1000000);
            $table->smallInteger('insurance_collision')->unsigned()->default(500);
            $table->smallInteger('insurance_comprehensive')->unsigned()->default(500);
            $table->enum('insurance_enforce', ['declaration', 'fax', 'red light', ''])->nullable();

            $table->string('insurance_fax', 15)->nullable();
            $table->integer('company_admin_user_id')->unsigned();
            $table->string('company_admin_phone', 32);
            $table->enum('driver_schedule', ['all_users', 'admin_super', 'super']);

            $table->boolean('start_stop_dates')->default(false);
            $table->boolean('insurance_dates')->default(false);
            $table->boolean('drivers_license_dates')->default(false);
            $table->enum('driver_country', ['US', 'CA', 'both'])->default('US');

            $table->smallInteger('capital_cost_adj')->default(0);
            $table->smallInteger('fuel_economy_adj')->default(0);
            $table->smallInteger('maintenance_adj')->default(0);
            $table->smallInteger('repair_adj')->default(0);

            $table->smallInteger('depreciation_adj')->default(0);
            $table->smallInteger('insurance_adj')->default(0);
            $table->smallInteger('fuel_price_adj')->default(0);
            $table->smallInteger('resale_value_adj')->default(0);

            $table->smallInteger('capital_cost_tax_adj')->default(0);
            $table->smallInteger('monthly_payment_adj')->default(0);
            $table->smallInteger('finance_cost_adj')->default(0);
            $table->smallInteger('fee_renewal_adj')->default(0);

            $table->decimal('fixed_adj', 7, 2)->default(0.00);
            $table->decimal('variable_adj', 6,2)->default(0.00);
            $table->tinyInteger('status_id')->unsigned()->default(1)->comment('1=active, 2=non-active');
            $table->boolean('demo_company')->default(false);

            $table->boolean('notify_about_insurance_expiry')->default(0);
            $table->integer('approved_by_user_id')->unsigned()->nullable();
            $table->dateTime('approved_date')->nullable();
            $table->boolean('quarterly_tax_adjustment')->default(true);

            $table->enum('insurance_responsibility', ['cardata', 'client', 'declaration'])->nullable();
            $table->string('license_enforce', 45)->nullable();
            $table->datetime('locked_date')->nullable();
            $table->integer('locked_user_id')->unsigned()->nullable();

            $table->string('locked_reason', 500)->nullable();
            $table->char('locked_type')->nullable()->default(null);
            $table->enum('licence_responsibility', ['cardata', 'client'])->nullable();
            $table->boolean('licence_reminder')->default(false);

            $table->string('licence_fax', 15)->nullable();
            $table->tinyInteger('draft_rates')->unsigned()->default(0);
            $table->tinyInteger('serv_carDATA')->unsigned()->default(1);
            $table->tinyInteger('serv_plan')->unsigned()->default(1)->comment('1=463, 2=FAVR, 3=canada, 4=consulting');

            $table->tinyInteger('serv_management')->unsigned()->default(1);
            $table->tinyInteger('serv_driver_calls')->unsigned()->default(1);
            $table->tinyInteger('serv_driver_changes')->unsigned()->default(1);
            $table->tinyInteger('serv_account_maintenance')->unsigned()->default(1);
            $table->tinyInteger('serv_mileage_inspection')->unsigned()->default(1);
            $table->tinyInteger('serv_traffic_light_status')->unsigned()->default(1);
            $table->tinyInteger('serv_mileage_recording')->unsigned()->default(1);
            $table->tinyInteger('serv_vehicle_resp')->unsigned()->default(1);
            $table->tinyInteger('serv_dl_documentation')->unsigned()->default(1);
            $table->tinyInteger('serv_mileage_band_audit')->unsigned()->default(1);
            $table->tinyInteger('serv_insurance_documentation')->unsigned()->default(1);
            $table->tinyInteger('serv_payment_responsibility')->unsigned()->default(1);

            $table->string('insurance_grace_period')->nullable()->comment('number of days or date in format yyyy-mm-dd (changed from original datetime field)');
            $table->string('licence_grace_period', 10)->nullable()->comment('number of days or date in format yyyy-mm-dd');
            $table->integer('cardata_admin_user_id')->unsigned()->default(0);
            $table->string('cardata_admin_phone', 32)->nullable();

            $table->tinyInteger('mileage_grace_period')->unsigned()->default(0)->comment('number of months driver can be late with mileage entry');
            $table->tinyInteger('insurance_grace_days')->unsigned()->default(0);
            $table->integer('insurance_medical')->unsigned()->nullable();
            $table->integer('motorist_person')->unsigned()->nullable();

            $table->integer('motorist_accident')->unsigned()->nullable();
            $table->smallInteger('fix_payment_advance')->unsigned()->default(0);
            $table->char('mileage_band_audit')->nullable()->comment('T=total, B=business mileage');
            $table->boolean('remove_fixed_without_insurance')->default(false);

            $table->datetime('remove_fixed_start_date')->nullable();
            $table->datetime('remove_fixed_end_date')->nullable();
            $table->boolean('IsDriverProfileEditable')->default(true);
            $table->boolean('IsShowManagerId')->default(false);

            $table->boolean('IsShowTaxLimit')->default(false);
            $table->boolean('IsShowQuarterlyTaxAudit')->default(true);
            $table->boolean('IsUsingJobTitle')->default(false);
            $table->tinyInteger('PayFileDate')->unsigned()->default(0);

            $table->boolean('AdvancedPayment')->default(0);
            $table->tinyInteger('licence_renewal_dd')->unsigned()->nullable();
            $table->tinyInteger('licence_renewal_mm')->unsigned()->nullable();
            $table->string('licence_transition_date', 10)->nullable();

            $table->integer('insurance_processor_id')->unsigned()->nullable();
            $table->integer('licence_processor_id')->unsigned()->nullable();
            $table->string('insurance_phone', 32)->nullable();
            $table->string('licence_phone', 32)->nullable();

            $table->tinyInteger('mileage_reminder_2')->default(0);
            $table->boolean('remove_reimbursement_without_insurance')->default(false);
            $table->string('restricted_country_access', 3)->nullable()->comment('??');
            $table->char('favr_insurance_renewal')->nullable()->default('A')->comment('A=annual declaration, M=when expired');

            $table->char('invoice_frequency')->nullable()->comment('M=monthly,Q=quarterly,A=annually');
            $table->decimal('fee_per_driver', 6, 2)->nullable();
            $table->smallInteger('no_reimbursement_reminder')->nullable()->comment('day of the month when we send email to company coordinator who did not get reimbursement and why');
            $table->boolean('hide_pdf_from_managers')->default(false)->comment('hide inks to insurance and license documents');

            $table->integer('miroute_admin_user_id')->unsigned()->nullable();
            $table->string('miroute_admin_phone', 32)->nullable();
            $table->boolean('use_stop_contract')->default(false);
            $table->boolean('no_odometer')->default(false);
            $table->boolean('use_commuter_mileage')->default(false);
            $table->date('use_commuter_mileage_transition_date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ap_company');
    }
}
