<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMiCompanyTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mi_company_tickets', function (Blueprint $table) {
            $table->integer('ticket_id')->unique()->unsigned();
            $table->integer('company_id')->unsigned();
            $table->foreign('company_id', 'ticket_company_fk')->references('company_id')->on('ap_company');
            $table->text('note');
            $table->dateTime('deleted_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mi_company_tickets', function (Blueprint $table) {
            $table->dropForeign('ticket_company_fk');
        });
        Schema::dropIfExists('mi_company_tickets');
    }
}
