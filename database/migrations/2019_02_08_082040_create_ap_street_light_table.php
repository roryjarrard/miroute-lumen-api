<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApStreetLightTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ap_street_light', function (Blueprint $table) {
            $table->increments('street_light_id');
            $table->enum('colour', ['yellow', 'green', 'red'])->default('yellow');
            $table->string('description')->default('new driver');
            $table->text('long_description');
            $table->boolean('exclude');
            $table->boolean('IsInsurance');
            $table->boolean('IsLicense');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ap_street_light');
    }
}
