<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDriverImportColumnsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('driver_import_columns', function (Blueprint $table) {
            $table->increments('id');
            $table->string('module', 32)->nullable();
            $table->string('column', 60);
            $table->string('label', 255);
            $table->boolean('mandatory')->default(false);
            $table->integer('order')->unsigned();
            $table->string('table',60);
            $table->boolean('copy_to')->default(false);
            $table->text('notes')->nullable();
            $table->string('column_type', 32)->default('string');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('driver_import_columns');
    }
}
