<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDriverReportingCenterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('driver_reporting_center', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('user_id')->unsigned();
            $table->foreign('user_id', 'ap_user_fk')->references('user_id')->on('ap_user');

            $table->integer('company_stop_id')->unsigned();
            $table->foreign('company_stop_id', 'company_stop_fk')->references('company_stop_id')->on('mi_company_saved_stop');

            $table->double('commute_distance')->nullable()->comment('One-way commute distance from home to the reporting center');

            $table->softDeletes();
            $table->timestamps();

            $table->unique(['user_id', 'deleted_at'], 'user_deleted_unique');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('driver_reporting_center', function (Blueprint $table) {
            $table->dropForeign('ap_user_fk');
            $table->dropForeign('company_stop_fk');
        });
        Schema::dropIfExists('driver_reporting_center');
    }
}
