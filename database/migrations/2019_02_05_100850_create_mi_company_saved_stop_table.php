<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMiCompanySavedStopTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mi_company_saved_stop', function (Blueprint $table) {
            $table->increments('company_stop_id');
            $table->integer('company_id')->unsigned();
            $table->string('name', 80);
            $table->string('address', 200);
            $table->string('city', 80)->nullable();
            $table->string('state_province', 2)->nullable();
            $table->string('zip_code', 10)->nullable();
            $table->string('country', 2)->nullable();
            $table->double('latitude');
            $table->double('longitude');
            $table->boolean('status')->default(true);
            $table->string('stop_number', 40)->nullable();
            $table->tinyInteger('reporting_center')->default(0);
            $table->index(['company_id', 'reporting_center'], 'reporting_center_idx');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mi_company_saved_stop');
    }
}
