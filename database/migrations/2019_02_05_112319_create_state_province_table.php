<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStateProvinceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('state_province', function (Blueprint $table) {
            $table->integer('state_province_id')->unsigned();
            $table->string('name');
            $table->string('abbreviation', 2);
            $table->enum('country', ['US', 'CA']);
            $table->unique(['state_province_id', 'country'], 'state_country_unique');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('state_province');
    }
}
