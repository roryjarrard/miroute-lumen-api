<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ap_user', function (Blueprint $table) {
            $table->increments('user_id');

            $table->smallInteger('company_id');
            $table->string('username', 12);
            $table->string('password', 255);
            $table->string('first_name', 32);
            $table->string('last_name', 32);
            $table->string('email', 64);
            $table->enum('active', ['active', 'inactive']);
            $table->enum('type', ['driver', 'manager', 'admin', 'super', 'sales']);
            $table->string('phone', 15)->nullable();
            $table->boolean('isApproved');

            $table->timestamps(); # todo: we need 'created_on' not 'created_at' per CDO
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ap_user');
    }
}
