<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMiCompanyContractsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mi_company_contracts', function (Blueprint $table) {
            $table->integer('contract_id')->unique()->unsigned();
            $table->integer('company_id')->unsigned();
            $table->foreign('company_id', 'contract_company_fk')->references('company_id')->on('ap_company');
            $table->text('note');
            $table->dateTime('deleted_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mi_company_contracts', function (Blueprint $table) {
            $table->dropForeign('contract_company_fk');
        });
        Schema::dropIfExists('mi_company_contracts');
    }
}
