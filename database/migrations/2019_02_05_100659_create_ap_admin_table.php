<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApAdminTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ap_admin', function (Blueprint $table) {
            $table->integer('user_id')->unsigned();
            $table->boolean('company_admin')->default(false);
            $table->boolean('company_approver')->default(false);
            $table->boolean('IsLimitedAdmin')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ap_admin');
    }
}
