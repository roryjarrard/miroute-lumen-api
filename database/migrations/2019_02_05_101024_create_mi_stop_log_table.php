<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMiStopLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mi_stop_log', function (Blueprint $table) {
            $table->increments('stop_log_id');
            $table->integer('user_id')->unsigned();
            $table->integer('company_stop_id')->unsigned()->nullable();
            $table->integer('driver_stop_id')->unsigned()->nullable();
            $table->enum('event_type', ['start','stopped','end'])->default('start');
            $table->string('business_purpose', 200)->nullable();
            $table->string('business_purpose_text', 400)->nullable();
            $table->double('mileage_original');
            $table->double('mileage_adjusted')->nullable();
            $table->string('mileage_comment', 200)->nullable();
            $table->date('trip_date');
            $table->tinyInteger('trip_sequence');
            $table->timestamp('time_started')->nullable();
            $table->timestamp('time_ended')->nullable();
            $table->timestamp('time_edited')->nullable();
            $table->unique(['user_id', 'trip_date', 'trip_sequence'], 'user_date_sequence');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mi_stop_log');
    }
}
