<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMiLoginTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mi_login', function (Blueprint $table) {
            $table->increments('login_id');
            $table->dateTime('time_stamp');
            $table->integer('user_id')->unsigned()->nullable();
            $table->string('username', 12);
            $table->string('ip', 15);
            $table->string('hostname', 100)->nullable();
            $table->enum('result', ['granted','invalid user','invalid password','inactive','not Mi-ROUTE','no profile','expired']);
            $table->string('token', 50)->nullable()->comment('not used, handled by jwt in api');
            $table->boolean('rememberMe')->nullable();
            $table->boolean('loggedIn')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mi_login');
    }
}
