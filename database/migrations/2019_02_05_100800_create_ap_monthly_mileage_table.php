<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApMonthlyMileageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ap_monthly_mileage', function (Blueprint $table) {
            $table->integer('user_id')->unsigned();
            $table->year('year');
            $table->smallInteger('jan_bus')->unsigned()->default(0);
            $table->mediumInteger('jan_odo')->unsigned()->default(0);
            $table->integer('jan_bus_pri')->unsigned()->default(0);
            $table->integer('jan_bus_alt')->unsigned()->default(0);
            $table->integer('jan_per')->unsigned()->default(0);
            $table->smallInteger('feb_bus')->unsigned()->default(0);
            $table->mediumInteger('feb_odo')->unsigned()->default(0);
            $table->integer('feb_bus_pri')->unsigned()->default(0);
            $table->integer('feb_bus_alt')->unsigned()->default(0);
            $table->integer('feb_per')->unsigned()->default(0);
            $table->smallInteger('mar_bus')->unsigned()->default(0);
            $table->mediumInteger('mar_odo')->unsigned()->default(0);
            $table->integer('mar_bus_pri')->unsigned()->default(0);
            $table->integer('mar_bus_alt')->unsigned()->default(0);
            $table->integer('mar_per')->unsigned()->default(0);
            $table->smallInteger('apr_bus')->unsigned()->default(0);
            $table->mediumInteger('apr_odo')->unsigned()->default(0);
            $table->integer('apr_bus_pri')->unsigned()->default(0);
            $table->integer('apr_bus_alt')->unsigned()->default(0);
            $table->integer('apr_per')->unsigned()->default(0);
            $table->smallInteger('may_bus')->unsigned()->default(0);
            $table->mediumInteger('may_odo')->unsigned()->default(0);
            $table->integer('may_bus_pri')->unsigned()->default(0);
            $table->integer('may_bus_alt')->unsigned()->default(0);
            $table->integer('may_per')->unsigned()->default(0);
            $table->smallInteger('jun_bus')->unsigned()->default(0);
            $table->mediumInteger('jun_odo')->unsigned()->default(0);
            $table->integer('jun_bus_pri')->unsigned()->default(0);
            $table->integer('jun_bus_alt')->unsigned()->default(0);
            $table->integer('jun_per')->unsigned()->default(0);
            $table->smallInteger('jul_bus')->unsigned()->default(0);
            $table->mediumInteger('jul_odo')->unsigned()->default(0);
            $table->integer('jul_bus_pri')->unsigned()->default(0);
            $table->integer('jul_bus_alt')->unsigned()->default(0);
            $table->integer('jul_per')->unsigned()->default(0);
            $table->smallInteger('aug_bus')->unsigned()->default(0);
            $table->mediumInteger('aug_odo')->unsigned()->default(0);
            $table->integer('aug_bus_pri')->unsigned()->default(0);
            $table->integer('aug_bus_alt')->unsigned()->default(0);
            $table->integer('aug_per')->unsigned()->default(0);
            $table->smallInteger('sep_bus')->unsigned()->default(0);
            $table->mediumInteger('sep_odo')->unsigned()->default(0);
            $table->integer('sep_bus_pri')->unsigned()->default(0);
            $table->integer('sep_bus_alt')->unsigned()->default(0);
            $table->integer('sep_per')->unsigned()->default(0);
            $table->smallInteger('oct_bus')->unsigned()->default(0);
            $table->mediumInteger('oct_odo')->unsigned()->default(0);
            $table->integer('oct_bus_pri')->unsigned()->default(0);
            $table->integer('oct_bus_alt')->unsigned()->default(0);
            $table->integer('oct_per')->unsigned()->default(0);
            $table->smallInteger('nov_bus')->unsigned()->default(0);
            $table->mediumInteger('nov_odo')->unsigned()->default(0);
            $table->integer('nov_bus_pri')->unsigned()->default(0);
            $table->integer('nov_bus_alt')->unsigned()->default(0);
            $table->integer('nov_per')->unsigned()->default(0);
            $table->smallInteger('dec_bus')->unsigned()->default(0);
            $table->mediumInteger('dec_odo')->unsigned()->default(0);
            $table->integer('dec_bus_pri')->unsigned()->default(0);
            $table->integer('dec_bus_alt')->unsigned()->default(0);
            $table->integer('dec_per')->unsigned()->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ap_monthly_mileage');
    }
}
