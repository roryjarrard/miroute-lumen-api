<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApAddressTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ap_address', function (Blueprint $table) {
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id', 'address_user_fk')->references('user_id')->on('ap_user');

            $table->enum('country', ['US', 'CA'])->default('US');
            $table->tinyInteger('state_id')->unsigned()->nullable();
            $table->tinyInteger('province_id')->unsigned()->nullable();
            $table->string('street', 64);
            $table->string('city', 32);
            $table->mediumInteger('zip')->unsigned()->nullable();
            $table->double('latitude')->default(0);
            $table->double('longitude')->default(0);
            $table->string('postal_code', 7)->nullable();
            $table->timestamp('time_stamp');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ap_address', function (Blueprint $table) {
            $table->dropForeign('address_user_fk');
        });
        Schema::dropIfExists('ap_address');
    }
}
