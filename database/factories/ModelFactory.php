<?php

use \Faker\Generator;
/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Generator $faker) {
    $companies = \App\Company::where('mileage_entry', 'miroute')->pluck('company_id')->toArray();
    return [
        'company_id' => $faker->randomElement($companies),
        'username' => strtolower(str_random(8)),
        'email' => $faker->email,
        'password' => app('hash')->make($faker->password),
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'active' => 'active',
        'type' => 'driver',
        'isApproved' => 1
    ];
});

$factory->define(App\Company::class, function (Generator $faker) {
    return [
        'name' => $faker->company,
        'mileage_entry' => $faker->randomElement(['monthly','monthly personal','daily log','weekly log','daily commuter','miroute']),
        'company_admin_user_id' => $faker->numberBetween(1000,40000),
        'company_admin_phone' => '000-000-0000 x000',
        'driver_country' => $faker->randomElement(['US', 'CA', 'both']),
        'cardata_admin_user_id' => $faker->numberBetween(1000,40000),
        'cardata_admin_phone' => '000-000-0000 x000',
        'miroute_admin_user_id' => $faker->numberBetween(1000,40000),
        'miroute_admin_phone' => '000-000-0000 x000',
        'use_stop_contract' => $faker->randomElement([0,1]),
        'no_odometer' => $faker->randomElement([0,1])
    ];
});

$factory->define(App\Driver::class, function (Generator $faker) {
    return [
        'user_id' => $faker->numberBetween(1000, 40000),
        'division_id' => $faker->numberBetween(500, 5000),
        'street_light_id' => 1,
        'vehicle_profile_id' => $faker->numberBetween(500, 1000),
        'mileage_band_id' => $faker->numberBetween(1, 10),
        'employee_num' => $faker->word,
        'territory' => $faker->randomElement(['home_city','home_state','multicity','multistate']),
        'territory_list' => '',
        'start_date' => $faker->date('Y-m-d'),
        'stop_date' => null,
        'cost_centre' => '',
        'business_unit' => '',
        'JobTitle' => ''
    ];
});



