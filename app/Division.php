<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Division extends Model
{
    protected $table = 'ap_division';
    protected $primaryKey = 'division_id';

    protected $fillable = [
        'company_id', 'name'
    ];

    public function company()
    {
        return $this->belongsTo('App\Company');
    }
}