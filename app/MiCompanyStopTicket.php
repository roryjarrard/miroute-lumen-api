<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MiCompanyStopTicket extends Model
{
    protected $table = 'mi_company_stop_ticket';

    protected $fillable = [
        'company_stop_id', 'ticket_id', 'deleted_at'
    ];
}