<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MiCompanySavedStop extends Model
{
    protected $table = 'mi_company_saved_stop';
    protected $primaryKey = 'company_stop_id';

    protected $fillable = [
        'company_id', 'name', 'address', 'city', 'state_province', 'zip_code', 'country', 'latitude', 'longitude',
        'status', 'stop_number', 'reporting_center'
    ];
}
