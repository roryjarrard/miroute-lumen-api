<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MiDriverSavedStop extends Model
{
    protected $table = 'mi_driver_saved_stop';
    protected $primaryKey = 'driver_stop_id';
    public $timestamps = false;

    protected $fillable = [
        'user_id', 'name', 'address', 'city', 'state_province', 'zip_code', 'country', 'latitude', 'longitude',
        'status', 'time_created'
    ];
}
