<?php

namespace App\Providers;

use App\Coordinator;
use App\DailyMileage;
use App\MiCompanySavedStop;
use App\MiDriverSavedStop;
use App\MiState;
use App\MiStopLog;
use App\StreetLight;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Facades\Hash;
use JWTAuth;
use App\User;
use App\Company;
use App\Driver;


class StatusServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Structure {
     *  'user_id',
     *  'json_data' => [
     *      'last_action' => '',
     *      'latest_stop_id' => '',
     *      'latest_stop_type' => '',
     *      'latest_stop_name' => '',
     *      'latest_page' => '',
     *      'previous_action' => '',
     *      'previous_stop_id' => '',
     *      'previous_stop_type' => '',
     *      'previous_stop_name' => '',
     *      'previous_page' => ''
     *  ],
     *  'updated_at' => ''
     * }
     * @param $user_id
     * @param string $dateTime
     * @param string $action
     * @param string $state
     */
    public static function updateState($user_id, $dateTime = '', $action = '', $state = '')
    {
        \Log::info('UPDATE STATE', compact('user_id', 'dateTime', 'action', 'state'));
        if ($dateTime == '')
            $dateTime = date('Y-m-d H:i:s');

        $dailyMileage = DailyMileage::where('user_id', $user_id)->orderBy('date_stamp', 'desc')->first();
        $stopLog = empty($dailyMileage) ? null : MiStopLog::where('user_id', $user_id)->where('trip_date', $dailyMileage->date_stamp)->orderBy('trip_sequence', 'desc')->first();

        // updated information
        $json_data = [
            'last_action' => $action,
            'latest_stop_id' => $stopLog->company_stop_id > 0 ? $stopLog->company_stop_id : $stopLog->driver_stop_id,
            'latest_stop_type' => $stopLog->company_stop_id > 0 ? 'company' : 'driver',
            'latest_stop_name' => $stopLog->company_stop_id > 0 ? MiCompanySavedStop::find($stopLog->company_stop_id)->name : MiDriverSavedStop::find($stopLog->driver_stop_id)->name,
            'latest_page' => $state
        ];

        // check if state has new structure
        $lastState = MiState::where('user_id', $user_id)->first();
        $lastJson = isset($lastState->json_data) ? json_decode($lastState->json_data, true) : null;

        if (is_null($lastJson) || !isset($lastJson['last_action']) || !isset($lastJson['latest_stop_id']) || !isset($lastJson['latest_stop_type'])
            || !isset($lastJson['latest_stop_name']) || !isset($lastJson['latest_page']))
            $lastJson = [
                'last_action' => '',
                'latest_stop_id' => '',
                'latest_stop_type' => '',
                'latest_stop_name' => '',
                'latest_page' => ''
            ];

        $json_data['previous_stop_id'] = $lastJson['latest_stop_id'];
        $json_data['previous_stop_type'] = $lastJson['latest_stop_type'];
        $json_data['previous_stop_name'] = $lastJson['latest_stop_name'];
        $json_data['previous_page'] = $lastJson['latest_page'];

        if (empty($lastState)) {
            MiState::create([
                'user_id' => $user_id,
                'json_data' => json_encode($json_data),
                'updated_at' => date('Y-m-d H:i:s')
            ]);
        } else {
            $lastState->update([
                'user_id' => $user_id,
                'json_data' => json_encode($json_data),
                'updated_at' => date('Y-m-d H:i:s')
            ]);
        }
    }

    public function updateCurrentPage($user_id, $action, $page, $userTime)
    {
        try {
            // check if state has new structure
            $lastState = MiState::where('user_id', $user_id)->first();
            $lastJson = !empty($lastState) && isset($lastState->json_data) ? json_decode($lastState->json_data, true) : null;

            if (is_null($lastJson) || !isset($lastJson['last_action']) || !isset($lastJson['latest_stop_id']) || !isset($lastJson['latest_stop_type'])
                || !isset($lastJson['latest_stop_name']) || !isset($lastJson['latest_page']))
                $lastJson = [
                    'last_action' => '',
                    'latest_stop_id' => '',
                    'latest_stop_type' => '',
                    'latest_stop_name' => '',
                    'latest_page' => ''
                ];

            $lastJson['last_action'] = $action;
            $lastJson['latest_page'] = $page;

            if (empty($lastState)) {
                MiState::create([
                    'user_id' => $user_id,
                    'json_data' => json_encode($lastJson),
                    'updated_at' => $userTime
                ]);
            } else {
                $lastState->update([
                    'user_id' => $user_id,
                    'json_data' => json_encode($lastJson),
                    'updated_at' => $userTime
                ]);
            }
        } catch (\Exception $e) {
            $statusCode = $e->getCode() >= 1000 ? 500 : $e->getCode(); // mysql errors most likely

            return [
                'success' => false,
                'title' => 'User Not Found',
                'message' => 'That email was not associated with any user in our records. Please try again.',
                'origin_message' => $e->getMessage(),
                'origin_status' => $e->getCode(),
                'statusCode' => $statusCode
            ];
        }

        return ['success' => true, 'message' => 'success', 'statusCode' => 201];
    }
}
