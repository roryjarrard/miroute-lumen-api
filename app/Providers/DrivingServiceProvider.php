<?php

namespace App\Providers;

use App\Coordinator;
use App\DailyMileage;
use App\MiCompanySavedStop;
use App\MiCompanyStopContract;
use App\MiCompanyStopTicket;
use App\MiDriverSavedStop;
use App\MiState;
use App\MiStopLog;
use App\MonthlyMileage;
use App\PersonalMileage;
use App\StreetLight;
use App\Trip;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Facades\Hash;
use JWTAuth;
use App\User;
use App\Stop;
use App\Company;
use App\Driver;

class DrivingServiceProvider extends ServiceProvider
{
    // defines if the last driving record was from 'today' or a previous date in time
    private $isToday = false;
    private $commuterServiceProvider;

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Top level request from controller
     *
     * @param $user_id
     * @param $userTime
     * @return array
     */
    public function getDriving($user_id, $userTime)
    {
        $userDate = $this->getUserDate($userTime);
        $miroute_page = $this->getMiroutePage($user_id, $userTime);

        // get a DailyMileage object from last driving date
        $lastDay = $this->getLastDrivingDay($user_id, $userDate);

        if (empty($lastDay))
            return [
                'last_ending_odometer' => null,
                'last_commute_mileage' => null,
                'last_entry_date' => null,
                'last_starting_odometer' => null,
                'last_business_mileage' => null,
                'lastStop' => null,
                'trips' => null,
                'miroute_page' => $miroute_page,
            ];

        $lastStop = $this->getLastStopInfo($user_id, $lastDay->date_stamp);
        $this->commuterServiceProvider = new CommuteServiceProvider(app());
        $lastCommuteMileage = $this->commuterServiceProvider->getCommuteMileage($user_id, $userTime);

        $trips = $this->getTripData($user_id, $lastDay->date_stamp);
        return [
            'last_ending_odometer' => $lastDay->ending_odometer,
            'last_ending_odometer_prompt' => $this->getLastEndingOdometer($user_id),
            'last_entry_date' => $lastDay->date_stamp,
            'last_starting_odometer' => $lastDay->starting_odometer,
            'last_business_mileage' => $lastDay->business_mileage,
            'last_commute_mileage' => $lastCommuteMileage['commute_mileage'],
            'show_commute_mileage' => $lastCommuteMileage['show_commute_mileage'],
            'commute_mileage_applies' => $this->commuterServiceProvider->commuteMileageApplies($user_id, $userDate),
            'lastStop' => $lastStop,
            'trips' => $trips,
            'miroute_page' => $miroute_page,
        ];
    }

    /**
     * DailyMileage info from the last driving day by user
     *
     * @param $user_id
     * @return mixed
     */
    public function getLastDrivingDay($user_id, $userDate)
    {
        $lastDay = DailyMileage::where('user_id', $user_id)->orderBy('date_stamp', 'desc')->first();

        // if the last record is from a day earlier than today, we need to generate a new record for today
        if ($lastDay->date_stamp < $userDate && $lastDay->ending_odometer > 0) {
            $lastDay = DailyMileage::create(['user_id' => $user_id, 'date_stamp' => $userDate]);
        }

        return $lastDay;
    }

    /**
     * Return a Stop instance of either a Company or Driver saved stop from the last day of driving by user.
     *
     * @param $user_id
     * @param $trip_date
     * @return Stop|null
     */
    public function getLastStopInfo($user_id, $trip_date)
    {
        $stopLog = MiStopLog::where('user_id', $user_id)->where('trip_date', $trip_date)->orderBy('trip_sequence', 'desc')->first();
        if (empty($stopLog))
            return null;

        # TODO if stop is null then what? they have daily_mileage started, but no starting trip
        $last_stop_id = $stopLog->company_stop_id > 0 ? $stopLog->company_stop_id : $stopLog->driver_stop_id;
        $last_stop_type = $stopLog->company_stop_id > 0 ? 'company' : 'driver';

        $savedStop = $last_stop_type == 'company' ? MiCompanySavedStop::find($last_stop_id) : MiDriverSavedStop::find($last_stop_id);

        $stop = new Stop([
            'id' => $last_stop_id,
            'type' => $last_stop_type,
            'name' => $savedStop->name,
            'street' => $savedStop->address,
            'latitude' => $savedStop->latitude,
            'longitude' => $savedStop->longitude
        ]);

        return $stop;
    }

    /**
     * Returns an array of Trip instances from the last day of driving by user
     *
     * @param $user_id
     * @param $trip_date
     * @return array|null
     */
    public function getTripData($user_id, $trip_date)
    {
        $stopLog = MiStopLog::where('user_id', $user_id)->where('trip_date', $trip_date)->get();

        if (empty($stopLog))
            return null;

        $trips = [];

        foreach ($stopLog as $log) {
            $trip = new Trip([]);
            $trip->business_purpose = $log->business_purpose_text;
            $trip->id = $log->stop_log_id;
            $trip->mileage_adjusted = $log->mileage_adjusted;
            $trip->mileage_comment = $log->mileage_comment;
            $trip->mileage_original = $log->mileage_original;
            $trip->trip_date = $log->trip_date;
            $trip->sequence_number = $log->trip_sequence;

            $savedStop = $log->company_stop_id > 0 ? MiCompanySavedStop::find($log->company_stop_id) : MiDriverSavedStop::find($log->driver_stop_id);
            $trip->stop = new Stop([]);
            $trip->stop->id = $log->company_stop_id > 0 ? $log->company_stop_id : $log->driver_stop_id;
            $trip->stop->type = $log->company_stop_id > 0 ? 'company' : 'driver';
            $trip->stop->name = $savedStop->name;
            $trip->stop->street = $savedStop->street;
            $trip->stop->latitude = $savedStop->latitude;
            $trip->stop->longitude = $savedStop->longitude;

            $trips[] = $trip;
        }

        return $trips;
    }

    /**
     * Returns the last entered ending odometer by the driver, regardless of date
     *
     * @param $user_id
     * @return mixed
     */
    public function getLastEndingOdometer($user_id)
    {
        return DailyMileage::where('user_id', $user_id)->whereNotNull('ending_odometer')->orderBy('date_stamp', 'desc')->first()->ending_odometer;
    }

    /**
     * Returns the last entered ending odometer by the driver before a specified date
     *
     * @param $user_id
     * @param $trip_date
     * @return mixed
     */
    public function getPreviousEndingOdometer($user_id, $trip_date)
    {
        return DailyMileage::where('user_id', $user_id)->where('date_stamp', '<', $trip_date)->whereNotNull('ending_odometer')->orderBy('date_stamp', 'desc')->first()->ending_odometer;
    }

    /**
     * Top level call to end a day with date and ending odometer
     *
     * @param $user_id
     * @param $ending_date
     * @param $ending_odometer
     * @param $userTime
     * @return array
     */
    public function updateEndingOdometer($user_id, $ending_date, $ending_odometer, $userTime)
    {
        $page = date('Y-m-d') == $ending_date ? 'day_ended' : 'start_day';
        try {
            // update ending odometer on ap_daily_mileage
            $dailyMileage = DailyMileage::where('user_id', $user_id)->where('date_stamp', $ending_date)->first();
            if (!empty($dailyMileage))
                $dailyMileage->update(['ending_odometer' => $ending_odometer]);
            else {
                return ['success' => true, 'miroute_page' => $page, 'drivingResource' => $this->getDriving($user_id, $userTime)];
            }

            // update last sequence for day and user to event_type=end
            $stopLog = MiStopLog::where('user_id', $user_id)->where('trip_date', $ending_date)->orderBy('trip_sequence', 'desc')->first();
            if (!empty($stopLog)) {
                $stopLog->update(['event_type' => 'end']);
            } else {
                // no stop log, just move to next step
                return ['success' => true, 'miroute_page' => $page, 'drivingResource' => $this->getDriving($user_id, $userTime)];
            }

            // if current_day: updateDailyMileage for business purpose, destination and business mileage
            $business_mileage = $this->updateDailyMileage($user_id, $ending_date, $ending_odometer);

            // updatePersonalMileage
            // endingOdometer will always calculate personal for the date specified
            $vehicle_changed = 0;
            $mileage = ($ending_odometer - $dailyMileage->starting_odometer) - $business_mileage;
            if ($mileage >= 1000 || $mileage < 0) {
                $mileage = 0;
                $vehicle_changed = 1;
            }
            $this->updatePersonalMileage($user_id, $ending_date, $mileage, 'personal', $vehicle_changed);

            # update state to current_day ? 'day_ended' : 'start_day'
            $userDate = $this->getUserDate($userTime);
            if ($ending_date == $userDate) {
                // this is today
                $page = 'day_ended';
                StatusServiceProvider::updateState($user_id, $userTime, 'ending_current_day', $page);
            } elseif ($ending_date < $userDate) {
                // previous day
                $page = 'start_day';
                StatusServiceProvider::updateState($user_id, $userTime, 'ending_previous_day', $page);
            }
        } catch (\Exception $e) {
            $statusCode = $e->getCode() >= 1000 ? 500 : $e->getCode(); // mysql errors most likely

            return [
                'success' => false,
                'title' => 'Unable to Update Odometer',
                'message' => 'We were unable to update your ending odometer at this time. Please try again, or contact your CSA for further assistance.',
                'origin_message' => $e->getMessage(),
                'origin_status' => $e->getCode(),
                'statusCode' => $statusCode
            ];
        }

        $drivingResource = $this->getDriving($user_id, $userTime);

        # return success w/ page
        return ['success' => true, 'miroute_page' => $page, 'drivingResource' => $drivingResource];
    }

    /**
     * Get combined destination, combined business purpose and combined mileage from mi_stop_log for given date
     *      and update the ap_daily_mileage table
     *
     * Calls update to monthly mileage
     *
     * @param $user_id
     * @param $trip_date
     * @param $odometer
     * @return int
     */
    public function updateDailyMileage($user_id, $trip_date, $odometer = null)
    {
        $combined_destination = '';
        $combined_business_purpose = '';
        $original_mileage_sum = 0;
        $original_adjusted_sum = 0;

        // get stop log entries for day and user
        $logEntries = MiStopLog::where('user_id', $user_id)->where('trip_date', $trip_date)->get();


        // if none then new start of day
        if (!is_null($logEntries)) {
            foreach ($logEntries as $log) {
                $original_mileage_sum += $log->mileage_original;
                $original_adjusted_sum += $log->mileage_adjusted;
                $combined_business_purpose .= $log->business_purpose_text . '; ';
                if (!empty($log->company_stop_id) && $log->company_stop_id > 0) {
                    $combined_destination .= MiCompanySavedStop::find($log->company_stop_id)->name . ' &rarr; ';
                } else {
                    $combined_destination .= MiDriverSavedStop::find($log->driver_stop_id)->name . ' &rarr; ';
                }
            }
        }

        // remove last empty elements
        $combined_destination_arr = explode(' &rarr; ', $combined_destination);
        array_pop($combined_destination_arr);
        $combined_destination = trim(implode(' &rarr; ', $combined_destination_arr));

        $combined_business_purpose_arr = explode('; ', $combined_business_purpose);
        array_pop($combined_business_purpose_arr);
        $combined_business_purpose = trim(implode('; ', $combined_business_purpose_arr));

        $business_mileage = $original_mileage_sum + $original_adjusted_sum;

        if (empty($business_mileage) || $business_mileage < 0) {
            $business_mileage = 0;
        }

        if (strlen($combined_business_purpose) > 500) {
            $combined_business_purpose = substr($combined_business_purpose, 0, 497) . '...';
        }

        if (strlen($combined_destination) > 500) {
            $combined_destination = substr($combined_destination, 0, 497) . '...';
        }

        $commuteMileage = $this->handleCommuteMileage($user_id, $trip_date);

        $business_mileage -= $commuteMileage;
        $business_mileage_rounded = round($business_mileage, 0);


        DailyMileage::where('user_id', $user_id)->where('date_stamp', $trip_date)->update([
            'destination' => $combined_destination,
            'business_purpose' => $combined_business_purpose,
            'business_mileage' => $business_mileage_rounded
        ]);

        // update ap_monthly_mileage
        if (!is_null($odometer)) {
            $this->updateMonthlyMileage($user_id, $trip_date, $odometer);
        }



        return $business_mileage;
    }

    public function handleCommuteMileage($user_id, $userDate)
    {
        $this->commuterServiceProvider = new CommuteServiceProvider(app());

        if ($this->commuterServiceProvider->commuteMileageApplies($user_id, $userDate)) {
            $result = $this->commuterServiceProvider->calculate($user_id, $userDate);

            if (!empty($result)) {
                $success = $this->commuterServiceProvider->storeDailyCommuteMileage($user_id, $userDate, $result->starting_commute, $result->ending_commute);

                if ($success) {
                    return $result->total_commute;
                }
            }
        }
        return 0;
    }

    /**
     * Update business mileage and odometer for user, year and month
     *
     * @param $user_id
     * @param $trip_date
     * @param $odometer
     */
    public function updateMonthlyMileage($user_id, $trip_date, $odometer)
    {
        // parse date for month
        $dateArray = explode('-', $trip_date);
        $dateYear = $dateArray[0];
        $dateMonth = $dateArray[1];
        $monthArray = [null, 'jan', 'feb', 'mar', 'apr', 'may', 'jun', 'jul', 'aug', 'sep', 'oct', 'nov', 'dec'];
        $column_name = $monthArray[$dateMonth / 1] . '_bus';
        $odometer_name = $monthArray[$dateMonth / 1] . '_odo';

        // span of dates from 1 to current day
        $from_date = $dateYear . '-' . $dateMonth . '-01';

        $business_mileage = DailyMileage::where('user_id', $user_id)->where('date_stamp', '>=', $from_date)->where('date_stamp', '<=', $trip_date)->sum('business_mileage');

        if (!(new UserServiceProvider(app()))->isDemoDriver($user_id)) {
            MonthlyMileage::where('user_id', $user_id)->where('year', $dateYear)->update([
                $column_name => $business_mileage,
                $odometer_name => $odometer
            ]);
        }
    }

    /**
     * Update personal mileage for the user
     *  - personal mileage during the day (pers1) when ending a day (submit ending odometer)
     *  - personal mileage between days (pers2, 'gap' mileage) when starting a new day (submit starting odometer)
     *
     * @param $user_id
     * @param $trip_date
     * @param int $mileage
     * @param string $type
     * @param int $vehicle_changed
     */
    public function updatePersonalMileage($user_id, $trip_date, $mileage = 0, $type = '', $vehicle_changed = 0)
    {
        $field = '';

        if (empty($type))
            return;

        if (!(new UserServiceProvider(app()))->isDemoDriver($user_id)) {
            if ($type == 'personal') {
                $field = 'personal_1';
                $other_field = 'personal_2';
            } elseif ($type == 'gap') {
                $field = 'personal_2';
                $other_field = 'personal_1';
            }

            if (empty($field))
                return;

            $personalMileage = PersonalMileage::where('user_id', $user_id)->where('entry_date', $trip_date)->first();
            if (empty($personalMileage)) {
                PersonalMileage::create([
                    'user_id' => $user_id,
                    'entry_date' => $trip_date,
                    $field => $mileage,
                    $other_field => 0,
                    'vehicle_changed' => $vehicle_changed
                ]);
            } else {
                $personalMileage->update(['user_id' => $user_id,
                    'entry_date' => $trip_date,
                    $field => $mileage,
                    $other_field => 0,
                    'vehicle_changed' => $vehicle_changed
                ]);
            }
        }
    }

    /**
     * Get the last ending odometer reading for the previous month
     *
     * @param $user_id
     * @param $year
     * @param $month
     * @return array
     */
    public function getLastMonthEndingOdometer($user_id, $year, $month)
    {
        $first_of_month = sprintf('%04d-%02d-01', $year, $month);
        $last_year = $month == 1 ? $year - 1 : $year;
        $last_month_full_name = $month == 1 ? 'December' : date('S', strtotime($first_of_month));
        $last_month_name_abbr = $month == 1 ? 'dec' : strtolower(date('M', strtotime($first_of_month)));
        $odometer_field = $last_month_name_abbr . '_odo';
        $last_odometer = MonthlyMileage::where('year', $last_year)->where('user_id', $user_id)->first()->{$odometer_field};

        return ['last_year' => $last_year, 'last_month' => $last_month_full_name, 'last_odometer' => $last_odometer];
    }

    public function getPersonalMileageDetails($user_id, $year, $month, $type, $last_odometer)
    {

    }

    public function submitStartingOdometer($user_id, $starting_odometer, $business_purpose, $stop_id, $stop_type, $userTime)
    {
        $userDate = $this->getUserDate($userTime);

        try {
            # get stop name from stop id
            $stop_name = $stop_type == 'driver' ? MiDriverSavedStop::find($stop_id)->name : MiCompanySavedStop::find($stop_id)->name;
            $dailyMileage = DailyMileage::where(['user_id' => $user_id, 'date_stamp' => $userDate])->first();

            # determine if there is ap_daily_mileage for "today" (from userTime)
            if (empty($dailyMileage)) {
                # if not create an initial record
                DailyMileage::create([
                    'user_id' => $user_id,
                    'date_stamp' => $userDate,
                    'destination' => $stop_name,
                    'business_purpose' => $business_purpose,
                    'starting_odometer' => $starting_odometer
                ]);
            } else {
                # update if exists
                $dailyMileage->update([
                    'destination' => $stop_name,
                    'business_purpose' => $business_purpose,
                    'starting_odometer' => $starting_odometer
                ]);
            }

            # insert into mi_stop_log
            $company_stop_id = $stop_type == 'company' ? $stop_id : null;
            $driver_stop_id = $stop_type == 'driver' ? $stop_id : null;
            MiStopLog::where('user_id', $user_id)->where('trip_date', $userDate)->delete();
            MiStopLog::create([
                'user_id' => $user_id,
                'company_stop_id' => $company_stop_id,
                'driver_stop_id' => $driver_stop_id,
                'event_type' => 'start',
                'business_purpose_text' => $business_purpose,
                'trip_date' => $userDate,
                'trip_sequence' => 1,
                'mileage_original' => 0.0,
                'mileage_adjusted' => 0.0,
                'time_started' => $userTime
            ]);

            # update state
            StatusServiceProvider::updateState($user_id, $userTime, 'starting odometer submitted', 'driving');

        } catch (\Exception $e) {
            $statusCode = $e->getCode() >= 1000 ? 500 : $e->getCode(); // mysql errors most likely

            return [
                'success' => false,
                'title' => 'Unable to Update Odometer',
                'message' => 'We were unable to update your starting odometer at this time. Please try again, or contact your CSA for further assistance.',
                'origin_message' => $e->getMessage(),
                'origin_status' => $e->getCode(),
                'statusCode' => $statusCode
            ];
        }

        $drivingResource = $this->getDriving($user_id, $userTime);

        # return success w/ page
        return ['success' => true, 'message' => 'success', 'drivingResource' => $drivingResource];
    }

    public function getNearbyStops($user_id, $latitude, $longitude, $userTime)
    {
        $userDate = $this->getUserDate($userTime);
        $company_id = User::find($user_id)->company_id;

        // max distance from current LatLng in km
        $max_distance = 0.5;

        // create container for stops to return
        $stops = [];

        try {
            // get driver stops for user
            $driverStops = MiDriverSavedStop::where('user_id', $user_id)->where('status', 1)->select(['driver_stop_id'])->get();

            // convert driver stops
            foreach ($driverStops as $dstop) {
                $stops[] = $this->convertStop($dstop->driver_stop_id, 'driver');
            }

            // get company stops for user's company
            $companyStops = MiCompanySavedStop::where('company_id', $company_id)->where('status', 1)->select(['company_stop_id'])->get();
            // convert company stops
            foreach ($companyStops as $cstop) {
                $stops[] = $this->convertStop($cstop->company_stop_id, 'company');
            }

            #die(json_encode($stops));

            // filter by distance
            $r = 6378.7; // radius of earth in km
            foreach ($stops as $k => $v) {
                $distance = $r * acos(sin($latitude / 57.2958) * sin($v->latitude / 57.2958) +
                        cos($latitude / 57.2958) * cos($v->latitude / 57.2958)
                        * cos($v->longitude / 57.2958 - $longitude / 57.2958));

                if ($distance > $max_distance) {
                    unset($stops[$k]);
                }
            }
        } catch (\Exception $e) {
            $statusCode = $e->getCode() >= 1000 ? 500 : $e->getCode(); // mysql errors most likely

            return [
                'success' => false,
                'title' => 'Unable to Get Nearby Stops',
                'message' => 'We are unable to find saved stops near your current location at this time. Please try again, or contact your CSA if you need further assistance.',
                'origin_message' => $e->getMessage(),
                'origin_status' => $e->getCode(),
                'statusCode' => $statusCode
            ];
        }

        return ['success' => true, 'message' => 'success', 'stops' => $stops, 'statusCode' => 200];
    }

    /**
     * Put into Stop model (structure Mi-Route is looking for)
     *
     * @param $stop_id
     * @param $stop_type
     * @return Stop
     */
    protected function convertStop($stop_id, $stop_type)
    {
        $rawStop = $stop_type == 'company' ? MiCompanySavedStop::find($stop_id) : MiDriverSavedStop::find($stop_id);
        if (empty($rawStop))
            die($stop_id . ' ' . $stop_type);

        $stop = new Stop([
            'id' => $stop_id,
            'name' => $rawStop->name,
            'type' => $stop_type,
            'latitude' => $rawStop->latitude,
            'longitude' => $rawStop->longitude,
            'street' => $rawStop->address,
            'contracts' => $stop_type == 'company' ? $this->getStopContracts($stop_id, $rawStop->company_id) : [],
            'tickets' => $stop_type == 'company' ? $this->getStopTickets($stop_id, $rawStop->company_id) : [],
        ]);

        return $stop;
    }

    public function submitStop($user_id, $stop_id, $stop_type, $business_purpose, $userTime)
    {
        $userDate = $this->getUserDate($userTime);

        try {

            // find country for user (used in mileage calculation)
            $country_id = User::find($user_id)->company->driver_country == 'CA' ? 2 : 1; // defaults to US

            // find stop to be submitted
            $stop = $stop_type == 'company' ? MiCompanySavedStop::find($stop_id) : MiDriverSavedStop::find($stop_id);

            // get last sequence number and stop number
            $lastStopLog = MiStopLog::where(['user_id' => $user_id, 'trip_date' => $userDate])->orderBy('trip_sequence', 'desc')->first();
            $lastStop = isset($lastStopLog->company_stop_id) && $lastStopLog->company_stop_id > 0 ? MiCompanySavedStop::find($lastStopLog->company_stop_id) : MiDriverSavedStop::find($lastStopLog->driver_stop_id);
            $next_sequence_number = $lastStopLog->trip_sequence + 1;

            // calculate the distance between this and previous stop
            $mileage_original = $this->getDrivingDistance($lastStop->latitude, $lastStop->longitude, $stop->latitude, $stop->longitude, $country_id);

            MiStopLog::create([
                'user_id' => $user_id,
                'company_stop_id' => $stop_type == 'company' ? $stop_id : null,
                'driver_stop_id' => $stop_type == 'driver' ? $stop_id : null,
                'event_type' => 'stopped',
                'business_purpose_text' => $business_purpose,
                'mileage_original' => $mileage_original,
                'mileage_adjusted' => 0.0,
                'mileage_comment' => '',
                'trip_date' => $userDate,
                'trip_sequence' => $next_sequence_number,
                'time_started' => date('Y-m-d H:i:s'), // todo: server time?
            ]);

            $this->updateDailyMileage($user_id, $userDate, null);

            StatusServiceProvider::updateState($user_id, $userTime, 'submitted stop', 'stopped');

        } catch (\Exception $e) {
            $statusCode = $e->getCode() >= 1000 ? 500 : $e->getCode(); // mysql errors most likely

            return [
                'success' => false,
                'title' => 'Unable to Submit Your Stop',
                'message' => 'We were unable to submit your stop at this time. Please try again, or contact your CSA for further assistance.',
                'origin_message' => $e->getMessage(),
                'origin_status' => $e->getCode(),
                'statusCode' => $statusCode
            ];
        }

        $drivingResource = $this->getDriving($user_id, $userTime);
        return ['success' => true, 'message' => 'success', 'drivingResource' => $drivingResource, 'statusCode' => 200];
    }

    public function submitNewStop($user_id, $stop, $userTime)
    {
        $stop = (object)$stop;

        try {
            $newStop = MiDriverSavedStop::create([
                'user_id' => $user_id,
                'name' => $stop->name,
                'address' => $stop->street,
                'city' => $stop->city,
                'state_province' => $stop->state,
                'zip_code' => $stop->zip_postal,
                'country' => $stop->country,
                'latitude' => $stop->latitude,
                'longitude' => $stop->longitude,
                'status' => 1,
                'time_created' => $userTime
            ]);
        } catch (\Exception $e) {
            $statusCode = $e->getCode() >= 1000 ? 500 : $e->getCode(); // mysql errors most likely

            return [
                'success' => false,
                'title' => 'Unable to Submit Your Stop',
                'message' => 'We were unable to submit your stop at this time. Please try again, or contact your CSA for further assistance.',
                'origin_message' => $e->getMessage(),
                'origin_status' => $e->getCode(),
                'statusCode' => $statusCode
            ];
        }

        return ['success' => true, 'message' => 'success', 'new_stop_id' => $newStop->driver_stop_id, 'statusCode' => 201];
    }

    public function updateTripMileage($user_id, $trip_id, $business_purpose, $mileage_adjusted, $mileage_comment, $userTime)
    {
        $userDate = $this->getUserDate($userTime);
        try {
            MiStopLog::where('stop_log_id', $trip_id)->first()->update([
                'business_purpose_text' => $business_purpose,
                'mileage_adjusted' => $mileage_adjusted,
                'mileage_comment' => $mileage_comment,
                'time_edited' => $userTime
            ]);

            $this->updateDailyMileage($user_id, $userDate);
        } catch (\Exception $e) {
            $statusCode = $e->getCode() >= 1000 ? 500 : $e->getCode(); // mysql errors most likely

            return [
                'success' => false,
                'title' => 'Unable to Update Mileage',
                'message' => 'We were unable to update your trip mileage at this time. Please try again, or contact your CSA for further assistance.',
                'origin_message' => $e->getMessage(),
                'origin_status' => $e->getCode(),
                'statusCode' => $statusCode
            ];
        }

        return ['success' => true, 'message' => 'success', 'drivingResource' => $this->getDriving($user_id, $userTime), 'statusCode' => 201];
    }

    /**
     * Returns the page that Mi-Route should redirect the user to
     *
     * @param $user_id
     * @param $userTime
     * @return string
     */
    public function getMiroutePage($user_id, $userTime)
    {
        $page = '';
        $userDate = !empty($userTime) ? $this->getUserDate($userTime) : date('Y-m-d');


        $dailyMileage = DailyMileage::where('user_id', $user_id)->orderBy('date_stamp', 'desc')->first();
        \Log::info('testing user ' . $userDate . ' vs daily mileage ' . $dailyMileage->date_stamp . ' with ending odo ' . $dailyMileage->ending_odometer);

        // set page default value based on daily mileage data
        if (is_null($dailyMileage)) {
            \Log::info('case 1');
            // new driver, no mileage
            $page = 'start_day'; // new driver
        } elseif ($dailyMileage->date_stamp == $userDate) {
            \Log::info("case 2");
            // today, either started or not
            if ($dailyMileage->starting_odometer > 0) {
                \Log::info('case 3');
                // either ended today already, or you've be
                if ($dailyMileage->ending_odometer > 0) {
                    \Log::info('case 4');
                    $page = 'day_ended';
                } else {
                    \Log::info('case 5');
                    $page = 'stopped';
                }
            } else {
                \Log::info('case 6');
                $page = 'start_day';
            }
        } else {
            // some day in the past

            // check if they ended that day
            if ($dailyMileage->ending_odometer > 0) {
                \Log::info('case 7');
                $page = 'start_day';
            } else {
                \Log::info('case 8');
                $page = 'end_previous_day';
            }
        }

        // now determine if specific page can be determined from MI_State
        $state = MiState::where('user_id', $user_id)->first();
        $data = is_null($state) ? null : json_decode($state->json_data, true);

        if (!is_null($data) && array_key_exists('latest_page', $data) && strlen($data['latest_page'])) {
            $page = $data['latest_page'];
        } else {
            $data['latest_page'] = $page;
            if (!is_null($state)) {
                $state->update(['json_data' => json_encode($data)]);
            } else {
                MiState::create(['user_id' => $user_id, 'json_data' => json_encode($data)]);
            }
            # TODO: should we update, or wipe out since we want new structure?
        }

        // now reset in the case of a previous day
        if ($dailyMileage->date_stamp < $userDate) {
            // check if they ended that day
            if ($dailyMileage->ending_odometer > 0) {
                \Log::info('case 9');
                $page = 'start_day';
            } else {
                \Log::info('case 10');
                $page = 'end_previous_day';
            }
        }

        return $page;
    }

    /**
     * Separate the date from the datetime object
     *
     * @param $userTime
     * @return mixed
     */
    protected function getUserDate($userTime)
    {
        return explode(' ', $userTime)[0];
    }


    protected function getDrivingDistance($from_lat, $from_lng, $to_lat, $to_lng, $country_id)
    {
        $key = env('GOOGLE_API_KEY');

        $url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=" . $from_lat . "," . $from_lng . "&destinations=" . $to_lat . "," . $to_lng . "&mode=driving&key=$key";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $response = curl_exec($ch);
        curl_close($ch);
        $response_a = json_decode($response, true);

        $dist = $response_a['rows'][0]['elements'][0]['distance']['value'];
        $dist *= $country_id == 1 ? 0.00062137 : 0.001; //converting meters to miles or km

        return round($dist, 1);
    }

    protected function getStopContracts($company_stop_id, $company_id, $include_inactive = false)
    {
        $use_stop_contract = Company::find($company_id)->use_stop_contract;
        if (!$use_stop_contract)
            return [];

        $whereArray = ['company_stop_id' => $company_stop_id];
        if (!$include_inactive)
            $whereArray['deleted_at'] = null;
        $rawContracts = MiCompanyStopContract::where($whereArray)->get();

        $contracts = [];
        foreach ($rawContracts as $c)
            if (!in_array($c, $contracts))
                $contracts[] = $c;

        return $contracts;
    }

    protected function getStopTickets($company_stop_id, $company_id, $include_inactive = false)
    {
        $use_stop_contract = Company::find($company_id)->use_stop_contract;
        if (!$use_stop_contract)
            return [];

        $whereArray = ['company_stop_id' => $company_stop_id];
        if (!$include_inactive)
            $whereArray['deleted_at'] = null;
        $rawTickets = MiCompanyStopTicket::where($whereArray)->get()->groupBy('company_stop_id', 'ticket_id')->toArray();

        $tickets = [];
        foreach ($rawTickets as $t)
            if (!in_array($t, $tickets))
                $tickets[] = $t;

        return $tickets;
    }
}
