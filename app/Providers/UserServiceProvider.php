<?php

namespace App\Providers;

use App\PasswordReset;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Facades\Hash;
use JWTAuth;
use App\User;
use App\Company;
use App\Driver;
use App\StreetLight;
use Illuminate\Support\Facades\Mail;
use App\Mail\CodeEmail;
use mysql_xdevapi\Exception;

class UserServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    public function registerUser($parameters = [])
    {
        try {
            $validator = Validator::make($parameters, [
                'company_id' => 'required|int',
                'password' => 'required|string|min:6|confirmed',
                'first_name' => 'required|string|max:32',
                'last_name' => 'required|string|max:32',
                'email' => 'required|string|email|max:64',
                'active' => 'required|in:active,inactive'
            ]);

            $user = User::create([
                'company_id' => $parameters['company_id'],
                'username' => $this->createUniqueUserName($parameters['first_name'], $parameters['last_name']),
                'password' => Hash::make($parameters['password']),
                'first_name' => $parameters['first_name'],
                'last_name' => $parameters['last_name'],
                'email' => $parameters['email'],
                'active' => $parameters['active'],
                'type' => 'driver',
                'isApproved' => 1
            ]);

            return $user;
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage(), 400);
        }
    }

    /**
     * Mi-Route resource for basic user information
     *
     * Normally show would take a user id, but we have the user object from the token
     *
     * Expecting these values
     *      - company_id
     *      - company_name
     *      - country
     *      - driverStatusItems
     *      - email
     *      - first_name
     *      - is_demo_driver
     *      - last_name
     *      - mileage_entry_method
     *      - mileage_label
     *      - street_light_color
     *      - street_light_id
     *      - use_stop_contract
     *      - use_tickets
     *      - user_id
     *      - username
     *
     * @param $user_id
     * @param $userTime
     * @return array
     */
    public function getUserResource($user_id, $userTime)
    {
        $user = User::find($user_id);
        $userDate = $this->getUserDate($userTime);

        $response_array = [];
        $street_light_id = $this->getStreetLightId($user_id);

        $response_array['company_id'] = $user->company_id;
        $response_array['email'] = $user->email;
        $response_array['first_name'] = $user->first_name;
        $response_array['last_name'] = $user->last_name;
        $response_array['user_id'] = $user_id;
        $response_array['username'] = $user->username;
        $response_array['street_light_id'] = $street_light_id;
        $response_array['street_light_color'] = $this->getStreetLightColor($user_id, $street_light_id);
        $response_array['driverStatusItems'] = $this->getStatusItems($user_id);

        $company = Company::where('company_id', $user->company_id)->first();

        $response_array['company_name'] = $company->name;
        $response_array['country'] = $company->driver_country;
        $response_array['mileage_entry_method'] = $company->mileage_entry;
        $response_array['mileage_label'] = $company->driver_country == 'US' ? 'mi' : 'km';
        $response_array['use_stop_contract'] = $company->use_stop_contract;
        $response_array['use_tickets'] = $this->isTicketDriver($user_id);

        $response_array['is_demo_driver'] = $this->isDemoDriver($user_id);

        return $response_array;
    }

    public function getStreetLightId($user_id)
    {
        return User::find($user_id)->driver->street_light_id;
    }

    public function getStreetLightColor($user_id, $street_light_id)
    {
        $street_light_id = $this->getStreetLightId($user_id);

        // test for scalar values
        if ($street_light_id & 1)
            return "yellow";
        if ($street_light_id == 2)
            return "green";

        foreach (StreetLight::all() as $light) {
            if (($street_light_id & $light->street_light_id) && $light->colour == 'red')
                return 'red';
        }
        return 'green';
    }

    /**
     * Return status items related to street light (license, insurance, etc.)
     * for notifications on Mi-Route dashboard
     *
     * @param $user_id
     * @return array
     */
    public function getStatusItems($user_id)
    {
        $items = [];

        $statusArray = $this->getDriverStreetLightArray($user_id);

        foreach ($statusArray['description'] as $desc) {
            if ($desc == 'reimbursement')
                $desc = 'You qualify for reimbursement';
            $items[] = ['color' => $statusArray['colour'], 'title' => $desc];
        }

        return $items;
    }


    protected function getDriverStreetLightArray($user_id)
    {
        $lightArray = ['colour' => '', 'description' => [], 'long_description' => []];

        $user = User::find($user_id);
        $street_light_id = $user->driver->street_light_id;
        $remove_reimbursement_without_insurance = $user->company->remove_reimbursement_without_insurance;

        $add_NIC = false;
        if ($remove_reimbursement_without_insurance) {
            if (($street_light_id & 16) > 0) { // 16=RED_AWAITING_INSURANCE
                $street_light_id -= 16;
                $add_NIC = true;
            }
            if (($street_light_id & 8) > 0) { // 8=RED_IMPROPER_INSURANCE
                $street_light_id -= 8;
                $add_NIC = true;
            }
            if (($street_light_id & 64) > 0) { // 64=RED_INSURANCE_EXPIRED
                $street_light_id -= 64;
                $add_NIC = true;
            }
            if ($add_NIC) {
                $street_light_id += 8192; // 8192=GREEN_NO_INSURANCE_COMPLIANCE
            }
        }
        #die('addnic ' . $add_NIC . $street_light_id);

        $lights = StreetLight::where('exclude', 0)->orderBy('street_light_id')->get();
        foreach ($lights as $light) {
            $match = ($street_light_id >= $light->street_light_id) && ($street_light_id & (int)$light->street_light_id);

            if ($match) {
                $lightArray['colour'] = $light->colour;
                $lightArray['description'][] = $light->description;
                $lightArray['long_description'][] = $light->long_description;
            }
        }


        $lightArray['colour'] = $add_NIC ? 'red' : $this->getStreetLightColor($user_id, $street_light_id);
        if ($add_NIC) {
            $lightArray['description'][] = 'You will not receive reimbursement while in Red Light Status.';
            $lightArray['long_description'][] = 'You will not receive reimbursement while in Red Light Status.';
        }

        return $lightArray;
    }

    /**
     * Only contract/ticket companies, and only from specific cost centers
     *
     * @param $user_id
     * @return bool
     */
    public function isTicketDriver($user_id)
    {
        $driver = Driver::where('user_id', $user_id)->first();
        if (!$driver)
            return false;
        return $driver->cost_centre == "Industrial";
    }

    /**
     * Demo drivers are trying the Mi-Route platform before officially starting on it
     *
     * @param $user_id
     * @return bool
     */
    public function isDemoDriver($user_id)
    {
        $user = User::find($user_id);
        if (!$user->driver)
            return false;

        $date = date('Y-m-d');

        // short circuit if not active
        if ($user->active != 'active')
            return false;


        // short circuit if yellow
        if ($user->driver->street_light_id === 1)
            return false;

        // short circuit if stop date is set
        $stop_date = $user->driver->stop_date;
        if (isset($stop_date) && ($stop_date != '0000-00-00' || $stop_date <= $date))
            return false;

        if ($user->company->mileage_entry != 'miroute')
            return false;


        if (!empty($user->miDriver) && isset($user->miDriver->enabled) && $user->miDriver->enabled === 0)
            return false;

        // finally the start date must be in the future
        return $user->driver->start_date > $date;
    }

    /**
     * We already tested that the company was NOT miroute before calling this function
     *
     * @param $user_id
     * @return bool
     */
    public function isTestDriver($user_id)
    {
        $user = User::find($user_id);
        if (!$user)
            return false;
        return !empty($user->miDriver) && $user->miDriver->enabled <= date('Y-m-d') && $user->miDriver->enabled = 1;
    }

    public function validateEmail($email)
    {
        try {
            $user = User::where('email', $email)->first();

            if (empty($user)) {
                throw new \Exception('That email was not associated with any user in our records. Please try again, or contact your CSA for further assistance.', 400);
            } else {
                $validation_code = rand(100000, 999999);
                PasswordReset::create([
                    'user_id' => $user->user_id,
                    'validation_code' => $validation_code,
                    'reset' => false
                ]);

                $this->sendEmail($user, $validation_code);
            }
        } catch (\Exception $e) {
            $statusCode = $e->getCode() >= 1000 ? 500 : $e->getCode(); // mysql errors most likely

            return [
                'success' => false,
                'title' => 'User Not Found',
                'message' => 'That email was not associated with any user in our records. Please try again, or contact your CSA for further assistance.',
                'origin_message' => $e->getMessage(),
                'origin_status' => $e->getCode(),
                'statusCode' => $statusCode
            ];
        }

        return ['success' => true, 'message' => 'success', 'statusCode' => 200];
    }

    public function validateCode($email, $code)
    {
        try {
            $user = User::where(['email' => $email, 'type' => 'driver'])->first();
            if (empty($user)) {
                return [
                    'success' => false,
                    'title' => 'Unable to Validate Code',
                    'message' => 'No validation code associated with the email ' . $email . ' found. Please try again, or contact your CSA for further assistance.',
                    'statusCode' => 400
                ];
            }

            $passwordReset = PasswordReset::where(['user_id' => $user->user_id, 'validation_code' => $code])->orderBy('created_at', 'desc')->first();

            if (empty($passwordReset)) {
                return [
                    'success' => false,
                    'title' => 'Unable to Validate Code',
                    'message' => 'No validation code associated with the email ' . $email . ' found. Please try again, or contact your CSA for further assistance.',
                    'statusCode' => 400
                ];
            } elseif (strtotime($passwordReset->created_at) < strtotime("-20 minutes")) {
                return [
                    'success' => false,
                    'title' => 'Unable to Validate Code',
                    'message' => 'That validation code has expired. Please submit your email for a new validation code.',
                    'statusCode' => 400
                ];
            }

            $passwordReset->reset = 1;
            $passwordReset->save();

        } catch (\Exception $e) {
            $statusCode = $e->getCode() >= 1000 ? 500 : $e->getCode(); // mysql errors most likely

            return [
                'success' => false,
                'title' => 'Unable to Validate Code',
                'message' => 'That code did not exist or has expired. Please try again or have a new code sent.',
                'origin_message' => $e->getMessage(),
                'origin_status' => $e->getCode(),
                'statusCode' => $statusCode
            ];
        }

        return ['success' => true, 'message' => 'success', 'statusCode' => 200];
    }

    public function submitNewPassword($email, $password)
    {
        try {
            $user = User::where(['email' => $email])->first();

            if (empty($user)) {
                return [
                    'success' => false,
                    'title' => 'Unable to Update Password',
                    'message' => 'No user associated with the email ' . $email . ' found. Please try again, or contact your CSA for further assistance.',
                    'statusCode' => 400
                ];
            }

            $user->password = password_hash($password, PASSWORD_BCRYPT);
            $user->save();
        } catch (\Exception $e) {
            $statusCode = $e->getCode() >= 1000 ? 500 : $e->getCode(); // mysql errors most likely

            return [
                'success' => false,
                'title' => 'Unable to Update Password',
                'message' => 'We are unable to update your password at this time. Please try again, or contact your CSA for further assistance.',
                'origin_message' => $e->getMessage(),
                'origin_status' => $e->getCode(),
                'statusCode' => $statusCode
            ];
        }

        return ['success' => true, 'message' => 'success', 'username'=>$user->username, 'statusCode' => 200];
    }

    /**
     * Create a unique username for CarData/MiRoute
     *
     * @param string $firstName
     * @param string $lastName
     * @return string
     */
    protected function createUniqueUserName($firstName = '', $lastName = '')
    {
        $try = 1;
        $potentialName = substr($firstName, 0, 1) . substr($lastName, 0, 10);
        while (User::where('username', $potentialName)->first()) {
            $try++;
            $potentialName = substr($firstName, 0, 1) . $try . substr($lastName, 0, 10 - strlen($try . ''));
        }
        return strtolower($potentialName);
    }

    protected function getUserDate($userTime)
    {
        return explode(' ', $userTime)[0];
    }

    protected function sendEmail($user, $validation_code)
    {
        #$email = $user->email;
        $email = 'roryjarrard@gmail.com';
        Mail::to($email)->send(new CodeEmail($user, $validation_code));
    }
}
