<?php

namespace App\Providers;

use App\Address;
use App\DailyCommuteMileage;
use App\DriverReportingCenter;
use App\MiCompanySavedStop;
use App\MiDriverSavedStop;
use App\MiStopLog;
use App\StateProvince;
use App\User;
use Illuminate\Support\ServiceProvider;
use App\Company;
use App\Driver;
use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;
use GuzzleHttp\Exception\ClientException;
use App\Mail\CommuterMileageEmail;

class CommuteServiceProvider extends ServiceProvider
{
    /**
     * This variable specifies the distance in meters within which a given location is considered "home"
     */
    private $considered_home_within = 500;
    private $rules_engine_url = 'https://rules-engine-nginx';

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * The calculate method is used to take a driver's day,
     * calculate the appropriate commute mileage.
     *
     * The result will either be NULL or, an array structured as follows:
     *
     * $result = [
     *      'business_mileage' => double,
     *      'starting_commute' => double,
     *      'ending_commute' => double
     * ]
     *
     * @param $rules_engine_url
     * @param $user_id
     * @param $date
     * @return array|NULL
     */
    public function calculate($user_id, $date)
    {
        try {
            // get data for rules engine

            $parameters = $this->getRequiredDataForCalculation($user_id, $date);

            // make the call to the rules engine
            $client = new Client([
                'verify' => false,
                'base_uri' => $this->rules_engine_url
            ]);
            try {
                $response = $client->post('/commute-mileage/calculate', [
                    RequestOptions::JSON => $parameters
                ]);
            } catch (ClientException $e) {

                // todo log
                $param_json = json_encode($parameters);
                $errors_json = json_encode($e->getResponse()->getBody());
                $exception_message = "Request params: $param_json \n Response: $errors_json";
                throw new \Exception($exception_message);
            }

            // get the result
            $result = json_decode($response->getBody());
            $status = $response->getStatusCode();

            // if something failed, throw an exception
            if ($status != 200) {

                throw new \Exception("Rules engine returned $status \n\n $result");
            }

        } catch (\Exception $e) {

            // todo what to do
            // email the dev team all exceptions
            $this->sendEmail('Commute Mileage Error', $e->getMessage());

            // return null to parent, to fail softly
            return null;
        }

        // all is well, so we return the result
        return $result;
    }

    /**
     * Determine based on the company (and the present date) if the company uses commuter mileage,
     *   1. is the company configured to "use_commuter_mileage"?
     *   2. is the commute_mileage_transition_date
     *
     * rjarrard: user_id not used because there isn't an exception here
     *
     * @param $user_id
     * @param $date
     * @return bool
     */
    function commuteMileageApplies($user_id, $date)
    {
        $company = Company::find(User::find($user_id)->company_id);
        $commute_applies = $company->use_commuter_mileage && ($company->use_commuter_mileage_transition_date <= $date);
        return $commute_applies;
    }

    public function getCommuteMileage($user_id, $userTime)
    {
        $userDate = self::getUserDate($userTime);
        $show_commute_mileage = false;
        $commute_mileage = 0;

        try {
            if ($this->commuteMileageApplies($user_id, $userDate)) {

                $commute = $this->getDailyCommuteMileage($user_id, $userDate);

                if (!empty($commute)) {
                    $starting_commute = isset($commute['starting_commute']) ? $commute['starting_commute'] : 0;
                    $ending_commute = isset($commute['ending_commute']) ? $commute['ending_commute'] : 0;

                    $commute_mileage = $starting_commute + $ending_commute;
                    $show_commute_mileage = true;
                }
            }
        } catch (\Exception $e) {
            $statusCode = $e->getCode() >= 1000 ? 500 : $e->getCode(); // mysql errors most likely

            return [
                'success' => false,
                'title' => 'Unable to Get Commute Mileage',
                'message' => 'We were unable to get commute mileage information at this time. Please try again, or contact your CSA if you need further assistance.',
                'origin_message' => $e->getMessage(),
                'origin_status' => $e->getCode(),
                'statusCode' => $statusCode
            ];
        }


        return [
            'success' => true,
            'message' => 'success',
            'show_commute_mileage' => $show_commute_mileage,
            'commute_mileage' => $commute_mileage,

            'statusCode' => 200
        ];
    }

    /**
     * Function gets the commute distance for the given user on the given date
     *
     * @param $user_id
     * @param $date
     * @return int
     * @throws \Exception
     */
    public function getCommuteDistance($user_id, $date)
    {
        if ($user_id < 0 || !self::validDate($date, 'Y-m-d')) {
            # todo what to do
            throw new \Exception('Invalid parameters for getCommuteDistance');
        }

        $driverReportingCenter = DriverReportingCenter::where('user_id', $user_id)
            ->where('created_at', '<=', $date)
            ->where('deleted_at', null)
            ->orWhere('deleted_at', '>', $date)
            ->orderBy('created_at')
            ->first();

        $commute_distance = empty($driverReportingCenter) ? 0 : $driverReportingCenter->commute_distance;

        return isset($commute_distance) ? $commute_distance : 0;
    }

    /**
     * Returns the commute mileage amounts for the given user on the given date
     *
     * @param $user_id
     * @param $date (Y-m-d)
     * @return array|null
     *      [
     *          'user_id' => x,
     *          'date' => y,
     *          'starting_commute' => z,
     *          'ending_commute' => a
     *      ]
     *
     * @throws \Exception
     */
    public function getDailyCommuteMileage($user_id, $date)
    {
        //validate
        if ($user_id < 0 || !self::validDate($date, 'Y-m-d')) {
            # todo what to do
            throw new \Exception('Invalid parameters for getDailyCommuteMileage');
        }

        $commuteMileage = DailyCommuteMileage::where(['user_id'=>$user_id, 'date'=>$date])->first();

        if (empty($commuteMileage)) {
            return null;
        } else {
            return $commuteMileage;
        }
    }

    /**
     * Based on the rules engine url, user_id, date and exception,
     * generate an email message
     *
     * Message parameter is the
     *
     * @param $user_id
     * @param $date
     * @param $message
     *      - message generated in code, not exception message
     * @param \Exception $e
     * @return string
     */
    private function getErrorEmailMessage($user_id, $date, $message, \Exception $e)
    {
        $current_date_and_time = date('Y-m-d H:i:s');

        $email_message = <<<EOQ
Commute mileage failed to calculate with the following parameters:
 - rules_engine_url:  $this->rules_engine_url
 - user_id:           $user_id
 - date:              $date
 
This occurred at $current_date_and_time

        {$message}

Exception information:
        {$e->getMessage()}
        
        {$e->getTraceAsString()}

EOQ;

        return $email_message;
    }

    public function getOfficeType($user_id, $date)
    {
        return self::slugify(Driver::where('user_id', $user_id)->first()->office_type);
    }

    /**
     * This function gathers all data required to hit the rules-engine and get
     * a commute mileage calculation for a given driver on a given day.
     *
     * It is tightly coupled to the API requirements of the commute-rules in the rules-engine
     * The resulting object will be of this format:
     *
     * {
     * "company": "a-b-c-enterprises",       (slugified)
     * "is_commute_company": true,           (boolean)
     * "number_of_trips": 2,                 (int)
     * "employee_type": "in-office",         (slugified in array ['remote', 'in-office'])
     * "state_province": "california",       (slugified)
     * "one_way_commute_distance": 10,       (double)
     * "trips": [                            (array)
     * {                                 (obj)
     * "started_at_home": false,         (boolean)
     * "ended_at_home": true,            (boolean)
     * "mileage": 0                      (double)
     * },
     * {
     * "started_at_home": false,
     * "ended_at_home": true,
     * "mileage": 0
     * }
     * ],
     * "total_mileage": 100                   (double)
     * }
     *
     * @param $user_id
     * @param $date
     * @return \stdClass
     * @throws \Exception
     */
    public function getRequiredDataForCalculation($user_id, $date)
    {
        $data = new \stdClass();
        $user = User::find($user_id);
        $company = $user->company;

        // get company name and is_commute_company
        $data->company = self::slugify($company->name);
        $data->is_commute_company = $company->use_commuter_mileage;

        // get the state province name
        $data->state_province = $this->getStateProvinceNameForUser($user_id, $date); // this will be empty string if no address for user

        // get the commute distance
        $data->one_way_commute_distance = $this->getCommuteDistance($user_id, $date);

        // get the employee type
        $data->employee_type = $this->getOfficeType($user_id, $date);

        // get the trips
        $data->trips = $this->getTripsFormattedForRulesEngine($user_id, $date);
        $data->number_of_trips = sizeof($data->trips);

        // calculate total mileage
        $data->total_mileage = 0;
        foreach ($data->trips as $trip)  {
            $data->total_mileage += $trip->mileage;
        }

        return $data;
    }

    /**
     * From the user_id and date, return the slugified state province name
     * associated with the user's address at the given point in time
     *
     * @param $user_id
     * @param $date
     * @return string
     * @throws \Exception
     */
    public function getStateProvinceNameForUser($user_id, $date, $slugify = true)
    {
        $address = Address::where('user_id', $user_id)->orderBy('time_stamp', 'desc')->first();
        if (empty($address)) {
            // todo what do do?
            //throw new \Exception('MySQL Error: ' . $this->mysqli->error);
            //throw new \Exception('Error getting user state/province from user id: ' . $user_id);
        }

        $state_province = empty($address) || empty($address->state_id) ? '' : StateProvince::where(['state_province_id' => $address->state_id, 'country' => $address->country])->first()->name;

        return $slugify ? self::slugify($state_province) : $state_province;
    }

    /**
     * Get the trips (formatted as the rules engine expects) for the given user
     * on the given day
     *
     * @param $user_id
     * @param $date
     * @return array
     * @throws \Exception
     */
    public function getTripsFormattedForRulesEngine($user_id, $date)
    {
        # TODO: review from CDO, quite a bit different

        $formattedTrips = [];
        $starting_latitude = 0;
        $starting_longitude = 0;

        $stopLogs = MiStopLog::where(['user_id'=>$user_id, 'trip_date' => $date])->orderBy('trip_sequence', 'asc')->get();

        if (empty($stopLogs))
            return $formattedTrips;

        $lastSequence = MiStopLog::where(['user_id'=>$user_id, 'trip_date'=>$date])->orderBy('trip_sequence', 'desc')->first()->trip_sequence;

        foreach ($stopLogs as $log) {
            $stop = !empty($log->company_stop_id) ? MiCompanySavedStop::find($log->company_stop_id) : MiDriverSavedStop::find($log->driver_stop_id);

            // there is not supposed to be mileage on the first trip
            if ($log->trip_sequence === 1) {
                $starting_latitude = $stop->latitude;
                $starting_longitude = $stop->longitude;
                continue;
            }

            $mileage = $log->mileage_original + $log->mileage_adjusted;

            // initialize a nice trip obj
            $tripObj = new \stdClass();
            // sum the mileage
            $tripObj->mileage = (double)$mileage;
            $tripObj->started_at_home = (int)false;
            $tripObj->ended_at_home = (int)false;


            /*
             * check if the first trip started at home.
             * We look back at the first MI_Stop_Log Record to see if this is the case
             */
            if ($log->trip_sequence == 1) {
                $tripObj->started_at_home = $this->isHome(
                    $user_id,
                    $date,
                    $starting_latitude,
                    $starting_longitude,
                    $this->considered_home_within
                );

                /*
                 * else we check the last trip to see if it ended at home.
                 */
            } else if ($log->trip_sequence === $lastSequence) {
                $tripObj->ended_at_home = $this->isHome(
                    $user_id,
                    $date,
                    $stop->latitude,
                    $stop->longitude,
                    $this->considered_home_within
                );
            }

            $formattedTrips[] = $tripObj;
        }

        return $formattedTrips;
    }

    /**
     * Given a user_id, a date, a coordinate pair and a radius of error,
     * this function returns a boolean stating whether the points can be considered
     * home for the user at the given point in time.
     *
     * @param $user_id
     * @param $date
     * @param $latitude
     * @param $longitude
     * @param $considered_home_within
     * @return bool
     * @throws \Exception
     */
    public function isHome($user_id, $date, $latitude, $longitude, $considered_home_within)
    {
        $address = Address::where('user_id', $user_id)->orderBy('time_stamp', 'desc')->first();

        if (empty($address)) {
            # todo: what to do
            // throw new \Exception('User ' . $user_id . ' had no address active on ' . $date);
        }

        // we have an address!
        $home_lat = $address->latitude;
        $home_lng = $address->longitude;

        $rad = M_PI / 180;
        //Calculate distance from latitude and longitude
        $theta = $home_lng - $longitude;
        $dist = sin($home_lat * $rad)
            * sin($latitude * $rad) + cos($home_lat * $rad)
            * cos($latitude * $rad) * cos($theta * $rad);

        // in meters!
        $distance_from_home = acos($dist) / $rad * 60 * 1853;

        return $distance_from_home <= $considered_home_within;
    }

    /**
     * Handles the storage of the daily commute mileage for a driver for a day.
     * Returns true if successful, false otherwise
     *
     * @param $user_id
     * @param $date
     * @param $starting_commute
     * @param $ending_commute
     * @return bool
     */
    public function storeDailyCommuteMileage($user_id, $date, $starting_commute, $ending_commute)
    {
        try {
            //validate
            if ($user_id < 0 || !self::validDate($date, 'Y-m-d') || $starting_commute < 0 || $ending_commute < 0) {
                # todo what to do

                throw new \Exception('Invalid parameters for storeDailyCommuteMileage');
            }

            $dailyCommuteMileage = $this->getDailyCommuteMileage($user_id, $date);

            if (!empty($dailyCommuteMileage)) {
                $dailyCommuteMileage->update(['starting_commute'=>$starting_commute, 'ending_commute'=>$ending_commute]);
            } else {
                DailyCommuteMileage::create([
                    'user_id'=>$user_id,
                    'date' => $date,
                    'starting_commute' => $starting_commute,
                    'ending_commute' => $ending_commute
                ]);
            }
        } catch (\Exception $e) {
            return false;
            //die(' aaa ' . $e->getMessage());
            # todo: what is our message
            //$this->sendEmail('Error Storing Commute Mileage', $e->getMessage());
        }

        // true if successful, false otherwise
        return true; #isset($result) ? $result : false;
    }

    /**
     * Given a date and a format this function will return a boolean representing
     * whether the given date adheres to the given format
     *
     * @param $date
     * @param string $format
     * @return bool
     */
    public static function validDate($date, $format = 'Y-m-d')
    {
        $d = date_create_from_format('Y-m-d', $date);
        return $d && $d->format($format) === $date;
    }

    /**
     * Returns a slugified version of the given string.
     * Not by reference, you must use the return value.
     *
     * @param $string
     * @return string
     */
    public static function slugify($string)
    {
        return strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $string)));
    }

    protected function sendEmail($subject, $message)
    {
        $email = 'build@cardataconsultants.com';
        Mail::to($email)->send(new CommuterMileageEmail($subject, $message));
    }

    /**
     * Separate the date from the datetime object
     *
     * @param $userTime
     * @return mixed
     */
    protected static function getUserDate($userTime)
    {
        return explode(' ', $userTime)[0];
    }
}
