<?php

namespace App\Providers;

use App\User;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\ServiceProvider;
use App\Providers\UserServiceProvider;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Carbon\Carbon;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Boot the authentication services for the application.
     *
     * @return void
     */
    public function boot()
    {
        // Here you may define how you wish users to be authenticated for your Lumen
        // application. The callback which receives the incoming request instance
        // should return either a User instance or null. You're free to obtain
        // the User instance via an API token or any other method necessary.

        $this->app['auth']->viaRequest('api', function ($request) {
            /*if ($request->input('api_token')) {
                return User::where('api_token', $request->input('api_token'))->first();
            }*/
            return app('auth')->setRequest($request)->user();
        });
    }

    /**
     * Login method
     *
     * @param array $credentials
     * @return array
     */
    static public function login($username, $passwowrd, $userTime)
    {
        $date = date('Y-m-d'); // server date
        $credentials = ['username'=>$username, 'password'=>$passwowrd];

        try {
            $user = User::where('username', $username)->first();

            #return response()->json(compact('request'));
            if (!$user)
                return [
                    'success' => false,
                    'title' => 'Unable to Log In',
                    'message' => "The username and password combination you entered doesn't match our records. Please try again.",
                    'statusCode' => 401
                ]; // Unauthorized

            if ($user->active == 'inactive')
                return [
                    'success' => false,
                    'title' => 'Unable to Log In',
                    'message' => "Your account has been inactivated. If you need assistance please contact your CSA or contact CarData Support.",
                    'statusCode' => 401
                ]; // Unauthorized
            if ($user->type !== 'driver')
                return [
                    'success' => false,
                    'title' => 'Unable to Log In',
                    'message' => "You are not allowed to use Mi-Route. Please use <a href='https://www.cardataconsultants.com'>www.cardataconsultants.com</a> instead.",
                    'statusCode' => 401
                ]; // Unauthorized

            // get status of this driver
            $userResource = (new UserServiceProvider(app()))->getUserResource($user->user_id, $userTime);

            if ($userResource['street_light_color'] == 'yellow')
                return [
                    'success' => false,
                    'title' => 'Unable to Log In',
                    'message' => "Please login to CarData online at <a href='https://www.cardataconsultants.com'>www.cardataconsultants.com</a> and set up your driver profile.",
                    'statusCode' => 401
                ]; // Unauthorized
            if ($userResource['street_light_id'] & 128 || $userResource['street_light_id'] & 256 || $userResource['street_light_id'] & 512)
                // 128=RED_ON_LEAVE, 256=RED_DISABILITY, 512=RED_TERMINATED
                return [
                    'success' => false,
                    'title' => 'Unable to Log In',
                    'message' => "Your account has been inactivated. If you need assistance please contact your CSA or contact CarData Support.",
                    'statusCode' => 401
                ]; // Unauthorized
            if (!empty($user->driver->stop_date) && $user->driver->stop_date <= $date)
                return [
                    'success' => false,
                    'title' => 'Unable to Log In',
                    'message' => "Your account has been inactivated. If you need assistance please contact your CSA or contact CarData Support.",
                    'statusCode' => 401
                ]; // Unauthorized

            // test for miroute, excluded or demo drivers
            if ($user->company->mileage_entry == 'miroute') {
                if ($user->driver->start_date > $date && !(new UserServiceProvider(app()))->isDemoDriver($user->user_id))
                    return [
                        'success' => false,
                        'title' => 'Unable to Log In',
                        'message' => "You are not allowed to use Mi-Route. Please use <a href='https://www.cardataconsultants.com'>www.cardataconsultants.com</a> instead.",
                        'statusCode' => 401
                    ]; // Unauthorized
            } else {
                if (!(new UserServiceProvider(app()))->isTestDriver($user->user_id))
                    return [
                        'success' => false,
                        'title' => 'Unable to Log In',
                        'message' => "You are not allowed to use Mi-Route. Please use <a href='https://www.cardataconsultants.com'>www.cardataconsultants.com</a> instead.",
                        'statusCode' => 401
                    ]; // Unauthorized
            }
        } catch (\Exception $e) {
            $statusCode = $e->getCode() >= 1000 ? 500 : $e->getCode(); // mysql errors most likely

            return [
                'success' => false,
                'title' => 'Unable to Log In',
                'message' => 'Unable to contact CarData at this time. If you need assistance please contact your CSA or contact CarData Support.',
                'origin_message' => $e->getMessage(),
                'origin_status' => $e->getCode(),
                'statusCode' => $statusCode
            ];
        }

        try {
            //$cdoUser = self::findCDOUser($user->user_id);

            if (!$token = JWTAuth::attempt($credentials, ['exp' => Carbon::now()->addDays(7)->timestamp]))
                return [
                    'success' => false,
                    'title' => 'Unable to Log In',
                    'message' => "The username and password combination you entered doesn't match our records. Please try again.",
                    'statusCode' => 401
                ]; // Unauthorized
        } catch (JWTException $e) {
            return [
                'success' => false,
                'title' => 'Unable to Log In',
                'message' => "The username and password combination you entered doesn't match our records. Please try again, or contact your CSA if you need further assistance.",
                'origin_message' => 'Could Not Create Token: ' . $e->getMessage(),
                'statusCode' => 500
            ];
        }

        # TODO: Log to MILogin

        $success = true;
        return ['success' => $success, 'message' => 'success', 'token' => $token];
    }

    static protected function findCDOUser($user_id) {
        $mysqli = new \mysqli('209.251.54.134', 'cardev', 'car99Mi78#/#', 'cardata_ca');
        $cdoUser = $mysqli->query("select * from AP_User where user_id = $user_id")->fetch_object();
        //die($cdoUser);
        if ($cdoUser) {
            // user exists in CDO, update password
            User::find($user_id)->update(['password' => \Hash::make($cdoUser->password)]);
        } else {
            User::create($cdoUser->toArray());
        }

        return $cdoUser;
    }
}
