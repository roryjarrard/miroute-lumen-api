<?php

namespace App\Providers;

use App\Coordinator;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Facades\Hash;
use JWTAuth;
use App\User;
use App\Company;
use App\Driver;

class CoordinatorServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    public static function getCoordinator($user_id, $userTime)
    {
        try {
            $coordinatorResource = [];

            foreach (['company', 'cardata', 'miroute'] as $type) {
                $coord = new Coordinator();
                $coord->create($type, User::find($user_id)->company->company_id);
                $coordinatorResource[$type] = $coord;
            }
        } catch (\Exception $e) {
            return ['success' => false, 'message' => $e->getMessage(), 'statusCode' => $e->getCode()];
        }

        return $coordinatorResource;
    }
}
