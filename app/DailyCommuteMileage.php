<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DailyCommuteMileage extends Model
{
    protected $table = 'daily_commute_mileage';
    protected $primaryKey = 'user_id';

    protected $fillable = [
        'user_id', 'date', 'starting_commute', 'ending_commute'
    ];
}
