<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MiLogin extends Model
{
    protected $table = 'mi_login';
    protected $primaryKey = 'login_id';

    protected $fillable = [
        'time_stamp', 'user_id', 'username', 'ip', 'hostname', 'result', 'token', 'rememberMe', 'loggedIn'
    ];
}