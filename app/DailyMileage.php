<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DailyMileage extends Model
{
    protected $table = 'ap_daily_mileage';
    protected $primaryKey = 'user_id';
    public $timestamps = false;

    protected $fillable = [
        'user_id', 'date_stamp', 'destination', 'business_purpose', 'starting_odometer', 'ending_odometer',
        'business_mileage', 'commuter_mileage'
    ];
}
