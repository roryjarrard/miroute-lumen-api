<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MiDriver extends Model
{
    protected $table = 'mi_driver';

    protected $fillable = [
        'user_id', 'enabled', 'mi_start_date'
    ];
}