<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StateProvince extends Model
{
    protected $table = 'state_province';

    protected $fillable = [
        'state_province_id', 'name', 'abbreviation', 'country'
    ];
}