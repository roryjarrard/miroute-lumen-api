<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MiCompanyContract extends Model
{
    protected $primaryKey = 'contract_id';

    protected $fillable = [
        'contract_id', 'company_id', 'note', 'deleted_at'
    ];

    public function company() {
        return $this->belongsTo('App\Company', 'company_id', 'company_id');
    }
}
