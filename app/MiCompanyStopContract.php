<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MiCompanyStopContract extends Model
{
    protected $table = 'mi_company_stop_contract';

    protected $fillable = [
        'company_stop_id', 'contract_id', 'deleted_at'
    ];
}