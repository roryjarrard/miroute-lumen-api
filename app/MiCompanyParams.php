<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MiCompanyParams extends Model
{
    protected $table = 'mi_company_params';
    protected $primaryKey = 'company_params_id';

    protected $fillable = [
        'company_id', 'param_name', 'param_value'
    ];
}