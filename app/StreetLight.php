<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StreetLight extends Model
{
    protected $table = 'ap_street_light';
    protected $primaryKey = 'street_light_id';

    protected $fillable = [
        'colour', 'description', 'long_description', 'exclude', 'IsInsurance', 'IsLicense'
    ];
}