<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DriverImportColumn extends Model
{
    protected $table = 'driver_import_columns';

    protected $fillable = [
        'module', 'column', 'label', 'mandatory', 'order', 'table', 'copy_to', 'notes', 'column_type'
    ];
}
