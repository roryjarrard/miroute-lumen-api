<?php

namespace App;

class Trip
{
    public $id;
    public $trip_date;
    public $sequence_number;
    public $business_purpose;
    public $mileage_original;
    public $mileage_adjusted;
    public $mileage_comment;
    public $stop;

    public function __construct(array $attributes)
    {
        $this->id = isset($attributes['id']) ? $attributes['id'] : null;
        $this->trip_date = isset($attributes['trip_date']) ? $attributes['trip_date'] : null;
        $this->trip_date = isset($attributes['sequence_number']) ? $attributes['id'] : null;
        $this->trip_date = isset($attributes['business_purpose']) ? $attributes['id'] : null;
        $this->trip_date = isset($attributes['mileage_original']) ? $attributes['id'] : null;
        $this->trip_date = isset($attributes['mileage_adjusted']) ? $attributes['id'] : null;
        $this->trip_date = isset($attributes['mileage_comment']) ? $attributes['id'] : null;
        $this->trip_date = isset($attributes['stop']) ? $attributes['stop'] : new Stop([]);
    }
}