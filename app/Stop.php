<?php

namespace App;

class Stop
{
    public $id;
    public $name;
    public $latitude;
    public $longitude;
    public $street;
    public $type;
    public $contracts;
    public $tickets;

    public function __construct(array $attributes)
    {
        $this->id = isset($attributes['id']) ? $attributes['id'] : null;
        $this->type = isset($attributes['type']) ? $attributes['type'] : null;
        $this->name = isset($attributes['name']) ? $attributes['name'] : null;
        $this->latitude = isset($attributes['latitude']) ? $attributes['latitude'] : null;
        $this->longitude = isset($attributes['longitude']) ? $attributes['longitude'] : null;
        $this->street = isset($attributes['street']) ? $attributes['street'] : null;
        $this->contracts = isset($attributes['contracts']) ? $attributes['contracts'] : [];
        $this->tickets = isset($attributes['tickets']) ? $attributes['tickets'] : [];
    }
}
