<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Driver extends Model
{
    protected $table = 'ap_driver';
    protected $primaryKey = 'user_id';
    public $timestamps = false;

    protected $fillable = [
        'user_id', 'division_id', 'street_light_id', 'vehicle_profile_id',
        'mileage_band_id', 'employee_num', 'territory', 'territory_list',
        'start_date', 'stop_date', 'insurance_date', 'drivers_license_date',
        'cost_centre', 'declaration_date', 'checked_liability_person', 'checked_liability_accident',
        'checked_property_damage', 'checked_deductible', 'checked_old_car', 'checked_business_use',
        'payroll_number', 'checked_disabled', 'checked_on_leave', 'checked_terminated',
        'checked_liability', 'checked_collision', 'checked_comprehensive', 'checked_not_qualified',
        'uploaded_licence_date', 'reference', 'business_unit', 'car_police_acceptance_date',
        'checked_medical', 'checked_motorist_person', 'checked_motorist_accident', 'checked_additional_insured',
        'JobTitle', 'remove', 'office_type'
    ];
}
