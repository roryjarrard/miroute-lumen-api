<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MonthlyMileage extends Model
{
    protected $table = 'ap_monthly_mileage';
    protected $primaryKey = 'user_id';
    public $timestamps = false;

    protected $fillable = [
        'user_id', 'year',
        'jan_bus', 'jan_bus_alt', 'jan_bus_pri', 'jan_odo', 'jan_per',
        'feb_bus', 'feb_bus_alt', 'feb_bus_pri', 'feb_odo', 'feb_per',
        'mar_bus', 'mar_bus_alt', 'mar_bus_pri', 'mar_odo', 'mar_per',
        'apr_bus', 'apr_bus_alt', 'apr_bus_pri', 'apr_odo', 'apr_per',
        'may_bus', 'may_bus_alt', 'may_bus_pri', 'may_odo', 'may_per',
        'jun_bus', 'jun_bus_alt', 'jun_bus_pri', 'jun_odo', 'jun_per',
        'jul_bus', 'jul_bus_alt', 'jul_bus_pri', 'jul_odo', 'jul_per',
        'aug_bus', 'aug_bus_alt', 'aug_bus_pri', 'aug_odo', 'aug_per',
        'sep_bus', 'sep_bus_alt', 'sep_bus_pri', 'sep_odo', 'sep_per',
        'oct_bus', 'oct_bus_alt', 'oct_bus_pri', 'oct_odo', 'oct_per',
        'nov_bus', 'nov_bus_alt', 'nov_bus_pri', 'nov_odo', 'nov_per',
        'dec_bus', 'dec_bus_alt', 'dec_bus_pri', 'dec_odo', 'dec_per',
    ];
}
