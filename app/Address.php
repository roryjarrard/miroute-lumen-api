<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    protected $table = 'ap_address';
    protected $primaryKey = 'user_id';

    protected $fillable = [
        'user_id', 'country', 'state_id', 'province_id', 'street', 'city',
        'zip', 'latitude', 'longitude', 'postal_code'
    ];
}
