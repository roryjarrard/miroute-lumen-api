<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Coordinator extends Model
{
    protected $fillable = ['email', 'first_name', 'last_name', 'phone'];
    public $email = '';
    public $first_name = '';
    public $last_name = '';
    public $phone = '';

    public function __construct($email=null, $first_name=null, $last_name=null, $phone=null)
    {
        if ($email)
            $this->setEmail($email);
        if ($first_name)
            $this->setFirstName($first_name);
        if ($last_name)
            $this->setLastName($last_name);
        if ($phone)
            $this->setPhone($phone);
    }

    public function setEmail($email)
    {
        $this->email = $email;
    }

    public function setFirstName($firstName)
    {
        $this->first_name = $firstName;
    }

    public function setLastName($lastName)
    {
        $this->last_name = $lastName;
    }

    public function setPhone($phone)
    {
        return $this->phone;
    }

    public function get() {
        return [
            'first_name' =>$this->first_name,
            'last_name' => $this->last_name,
            'email' => $this->email,
            'phone' => $this->phone
        ];
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function getFirstName()
    {
        return $this->first_name;
    }

    public function getLastName()
    {
        return $this->last_name;
    }

    public function getPhone()
    {
        return $this->phone;
    }

    public function create($type = '', $company_id = '') {
        $company = Company::find($company_id);

        switch ($type) {
            case 'company':
                $admin = User::where('user_id', $company->company_admin_user_id)->first();
                $this->setFirstName($admin->first_name);
                $this->setLastName($admin->last_name);
                $this->setEmail($admin->email);
                $this->setPhone($company->company_admin_phone);
                break;
            case 'cardata':
                $admin = User::where('user_id', $company->cardata_admin_user_id)->first();
                $this->setFirstName($admin->first_name);
                $this->setLastName($admin->last_name);
                $this->setEmail($admin->email);
                $this->setPhone($company->cardata_admin_phone);
                break;
            case 'miroute':
                $admin = User::where('user_id', $company->miroute_admin_user_id)->first();
                $this->setFirstName($admin->first_name);
                $this->setLastName($admin->last_name);
                $this->setEmail($admin->email);
                $this->setPhone($company->miroute_admin_phone);
                break;
        }
    }

    public function toArray() {
        return [
            'user_id' => $this->user_id,
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'email' => $this->email,
            'phone' => $this->phone,
        ];
    }
}
