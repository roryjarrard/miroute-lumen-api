<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @method static where(string $string, $user_id)
 */
class DriverReportingCenter extends Model
{
    protected $table = 'driver_reporting_center';

    protected $fillable = [
        'user_id', 'company_admin', 'company_approver', 'isLimitedAdmin'
    ];
}
