<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Model implements AuthenticatableContract, AuthorizableContract, JWTSubject
{
    use Authenticatable, Authorizable;

    protected $table = 'ap_user';
    protected $primaryKey = 'user_id';

    /**
     * The name of the "created at" column.
     *
     * @var string
     */
    const CREATED_AT = 'created_on';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    # TODO: had to make user_id fillable so we can bring new users in from CDO in AuthServiceProvider::getCDOUser
    #   - but this is bad and we need a new solution
    protected $fillable = [
        'user_id', 'company_id', 'username', 'password', 'first_name', 'last_name',
        'email', 'active', 'type', 'phone', 'isApproved'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function company() {
        return $this->belongsTo('App\Company', 'company_id');
    }

    public function driver() {
        if ($this->type == 'driver') {
            return $this->belongsTo('App\Driver', 'user_id', 'user_id');
        } else {
            return null;
        }
    }

    public function miDriver() {
        if ($this->type == 'driver') {
            return $this->belongsTo('App\MiDriver', 'user_id', 'user_id');
        } else {
            return null;
        }
    }
}
