<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MiStopLog extends Model
{
    protected $table = 'mi_stop_log';
    protected $primaryKey = 'stop_log_id';
    public $timestamps = false;

    protected $fillable = [
        'user_id', 'company_stop_id', 'driver_stop_id', 'event_type', 'business_purpose', 'business_purpose_text',
        'mileage_original', 'mileage_adjusted', 'mileage_comment', 'trip_date', 'trip_sequence', 'time_started', 'time_ended', 'time_edited'
    ];
}
