<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @method static find($company_id)
 */
class Company extends Model
{
    protected $table = 'ap_company';
    protected $primaryKey = 'company_id';

    protected $fillable = ['name', 'mileage_reminder', 'mileage_lock_driver',
        'mileage_lock_all', 'mileage_entry', 'direct_pay_date', 'direct_pay_file_fee',
        'direct_pay_driver_fee', 'approval_end_date', 'car_policy_pdf', 'car_policy_enforce',
        'insurance_person', 'insurance_accident', 'insurance_property', 'insurance_deductible',
        'insurance_plpd', 'insurance_collision', 'insurance_comprehensive', 'insurance_enforce',
        'insurance_fax', 'company_admin_user_id', 'company_admin_phone', 'driver_schedule',
        'start_stop_dates', 'insurance_dates', 'drivers_license_dates', 'driver_country',
        'capital_cost_adj', 'fuel_economy_adj', 'maintenance_adj', 'repair_adj',
        'depreciation_adj', 'insurance_adj', 'fuel_price_adj', 'resale_value_adj',
        'capital_cost_tax_adj', 'monthly_payment_adj', 'finance_cost_adj', 'fee_renewal_adj',
        'fixed_adj', 'variable_adj', 'status_id', 'demo_company',
        'notify_about_insurance_expiry', 'approved_by_user_id', 'approved_date', 'quarterly_tax_adjustment',
        'insurance_responsibility', 'license_enforce', 'locked_date', 'locked_user_id',
        'locked_reason', 'locked_type', 'licence_responsibility', 'licence_reminder',
        'licence_fax', 'draft_rates', 'serv_carDATA', 'serv_plan',
        'serv_management', 'serv_driver_calls', 'serv_driver_changes', 'serv_account_maintenance',
        'serv_mileage_inspection', 'serv_traffic_light_status', 'serv_mileage_recording', 'serv_vehicle_resp',
        'serv_dl_documentation', 'serv_mileage_band_audit', 'serv_insurance_documentation', 'serv_payment_responsibility',
        'insurance_grace_period', 'licence_grace_period', 'cardata_admin_user_id', 'cardata_admin_phone',
        'mileage_grace_period', 'insurance_grace_days', 'insurance_medical', 'motorist_person',
        'motorist_accident', 'fix_payment_advance', 'mileage_band_audit', 'remove_fixed_without_insurance',
        'remove_fixed_start_date', 'remove_fixed_end_date', 'IsDriverProfileEditable', 'IsShowManagerId',
        'IsShowTaxLimit', 'IsShowQuarterlyTaxAudit', 'IsUsingJobTitle', 'PayFileDate',
        'AdvancedPayment', 'licence_renewal_dd', 'licence_renewal_mm', 'licence_transition_date',
        'insurance_processor_id', 'licence_processor_id', 'insurance_phone', 'licence_phone',
        'mileage_reminder_2', 'remove_reimbursement_without_insurance', 'restricted_country_access', 'favr_insurance_renewal',
        'invoice_frequency', 'fee_per_driver', 'no_reimbursement_reminder', 'hide_pdf_from_managers',
        'miroute_admin_user_id', 'miroute_admin_phone', 'use_stop_contract', 'no_odometer', 'use_commuter_mileage', 'use_commuter_mileage_transition_date'
];

    public function mirouteParams() {
        if ($this->mileage_entry != 'miroute') {
            return null;
        }
        return $this->hasMany('App\MiCompanyParams', 'company_id', 'company_id');
    }
}
