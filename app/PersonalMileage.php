<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PersonalMileage extends Model
{
    protected $table = 'ap_personal_mileage';
    protected $primaryKey = 'user_id';
    public $timestamps = false;

    protected $fillable = [
        'user_id', 'entry_date', 'personal_1', 'personal_2', 'vehicle_changed'
    ];
}
