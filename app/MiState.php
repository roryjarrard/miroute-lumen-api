<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MiState extends Model
{
    protected $table = 'mi_state';
    protected $primaryKey = 'user_id';
    public $timestamps = false;

    protected $fillable = [
        'user_id', 'json_data'
    ];
}
