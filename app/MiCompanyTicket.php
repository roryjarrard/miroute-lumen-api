<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MiCompanyTicket extends Model
{
    protected $primaryKey = 'ticket_id';

    protected $fillable = [
        'ticket_id', 'company_id', 'note', 'deleted_at'
    ];

    public function company() {
        return $this->belongsTo('App\Company', 'company_id', 'company_id');
    }
}
