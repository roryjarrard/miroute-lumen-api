<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Providers\AuthServiceProvider;

class AuthController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login']]);
    }

    /**
     * Authenticate with username and password
     * - user must be a driver
     * - user must be active
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        $userTime = $request->has('userTime') ? $request->get('userTime') : date('Y-m-d H:i:s');

        try {
            $response = AuthServiceProvider::login($request->get('username'), $request->get('password'), $userTime);
        } catch (\Exception $e) {
            return response()->json([
                'title' => 'Unable to Log In',
                'message' => 'Unable to contact CarData at this time. If you need assistance please contact your CSA or contact CarData Support.',
                'origin_message' => $e->getMessage(),
                'origin_status' => $e->getCode()
            ], 500);
        }

        if (isset($response['success']) && $response['success']) {
            return response()->json(['message' => $response['message'], 'token' => $response['token']], 200);
        } else {
            $statusCode = !isset($response['statusCode']) || empty($response['statusCode']) ? 500 : $response['statusCode'];
            return response()->json([
                'title' => $response['title'],
                'message' => $response['message'],
                'origin_message' => isset($response['origin_message']) ? $response['origin_message'] : $response['message'],
                'origin_status' => isset($response['origin_status']) ? $response['origin_status'] : $statusCode
            ], $statusCode);
        }
    }

    public function logout(Request $request)
    {
        # TODO: need to log this, and revoke token if possible
        return response()->json(['message' => 'logged out'], 200);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refreshToken()
    {
        return $this->respondWithToken(Auth::refresh());
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => Auth::factory()->getTTL() * 60 * 24 * 7
        ]);
    }
}
