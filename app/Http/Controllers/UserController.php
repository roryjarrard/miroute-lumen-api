<?php

namespace App\Http\Controllers;

use App\Providers\CoordinatorServiceProvider;
use App\Providers\CommuteServiceProvider;
use App\Providers\UserServiceProvider;
use Illuminate\Http\Request;
use JWTAuth;

class UserController extends Controller
{
    private $userServiceProvider;

    public function __construct()
    {
        $this->userServiceProvider = new UserServiceProvider(app());
    }

    /**
     * Create a new user.
     * - type: driver only
     *
     * This will only be used for development purposes. All "real" users must be
     *  created in the backend system
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(Request $request)
    {
        try {
            $user = $this->userServiceProvider->registerUser($request->all());
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], $e->getCode());
        }
        $token = JWTAuth::fromUser($user);

        return response()->json(compact('user', 'token'), 201);
    }

    /**
     * See UserServiceProvider for expected return values
     *
     * @param Request $request
     * @return false|string
     */
    public function getUserResource(Request $request)
    {
        $userResource = null;

        try {
            $userResource = $this->userServiceProvider->getUserResource($request->user->user_id, $request->userTime);
        } catch (\Exception $e) {
            return response()->json([
                'title' => 'Unable to Get User Information',
                'message' => 'We are unable to get your user information at this time. Please try again, or contact your CSA for further assistance.',
                'origin_message' => $e->getMessage(),
                'origin_status' => $e->getCode()
            ], 500);
        }

        return json_encode($userResource);
    }

    public function getCoordinatorResource(Request $request)
    {

        try {
            $coord = CoordinatorServiceProvider::getCoordinator($request->user->user_id, $request->userTime);
        } catch (\Exception $e) {
            return response()->json([
                'title' => 'Unable to Get Coordinator Information',
                'message' => 'We are unable to get your coordinator information at this time. Please try again, or contact your CSA for further assistance.',
                'origin_message' => $e->getMessage(),
                'origin_status' => $e->getCode()
            ], 500);
        }

        return json_encode($coord);
    }

    /**
     * Validate submitted email to then generate code for password reset
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function validateEmail(Request $request)
    {
        try {
            $response = $this->userServiceProvider->validateEmail($request->get('email'));
        } catch (\Exception $e) {
            return response()->json([
                'title' => 'Unable to Validate Email',
                'message' => 'We are unable to validate your email at this time. Please try again, or contact your CSA for further assistance.',
                'origin_message' => $e->getMessage(),
                'origin_status' => $e->getCode()
            ], 500);
        }

        if (isset($response['success']) && $response['success']) {
            return response()->json(['message' => $response['message']], 200);
        } else {
            $statusCode = !isset($response['statusCode']) || empty($response['statusCode']) ? 500 : $response['statusCode'];
            return response()->json([
                'title' => $response['title'],
                'message' => $response['message'],
                'origin_message' => isset($response['origin_message']) ? $response['origin_message'] : $response['message'],
                'origin_status' => isset($response['origin_status']) ? $response['origin_status'] : $statusCode,
            ], $statusCode);
        }
    }

    /**
     * Validate submitted code to then allow reset password
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function validateCode(Request $request)
    {
        try {
            $response = $this->userServiceProvider->validateCode($request->get('email'), $request->get('code'));
        } catch (\Exception $e) {
            return response()->json([
                'title' => 'Unable to Validate Code',
                'message' => 'We are unable to validate your code at this time. Please try again, or contact your CSA for further assistance.',
                'origin_message' => $e->getMessage(),
                'origin_status' => $e->getCode()
            ], 500);
        }

        if (isset($response['success']) && $response['success']) {
            return response()->json(['message' => $response['message']], 200);
        } else {
            $statusCode = !isset($response['statusCode']) || empty($response['statusCode']) ? 500 : $response['statusCode'];
            return response()->json([
                'title' => $response['title'],
                'message' => $response['message'],
                'origin_message' => isset($response['origin_message']) ? $response['origin_message'] : $response['message'],
                'origin_status' => isset($response['origin_status']) ? $response['origin_status'] : $statusCode,
            ], $statusCode);
        }
    }

    /**
     * Receive new password to be set on user
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function submitNewPassword(Request $request)
    {
        try {
            $response = $this->userServiceProvider->submitNewPassword($request->get('email'), $request->get('password'));
        } catch (\Exception $e) {
            return response()->json([
                'title' => 'Unable to Update Password',
                'message' => 'We are unable to update your password at this time. Please try again, or contact your CSA for further assistance.',
                'origin_message' => $e->getMessage(),
                'origin_status' => $e->getCode()
            ], 500);
        }

        if (isset($response['success']) && $response['success']) {
            return response()->json(['message' => $response['message']], 200);
        } else {
            $statusCode = !isset($response['statusCode']) || empty($response['statusCode']) ? 500 : $response['statusCode'];
            return response()->json([
                'title' => $response['title'],
                'message' => $response['message'],
                'origin_message' => isset($response['origin_message']) ? $response['origin_message'] : $response['message'],
                'origin_status' => isset($response['origin_status']) ? $response['origin_status'] : $statusCode,
            ], $statusCode);
        }
    }

    public function getOfficeType(Request $request)
    {
        $commuterProvider = new CommuteServiceProvider(app());
        $date = explode(' ', $request->userTime)[0];
        try {
            $data = $commuterProvider->getOfficeType($request->user->user_id, $date);
            return $data;
        } catch (\Exception $e) {
            die($e->getMessage());
        }

    }
}
