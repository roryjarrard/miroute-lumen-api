<?php

namespace App\Http\Controllers;

use App\Providers\CommuteServiceProvider;
use App\Providers\DrivingServiceProvider;
use Illuminate\Http\Request;

class DrivingController extends Controller
{
    private $drivingService;

    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => []]);
        $this->drivingService = new DrivingServiceProvider(app());
    }


    /**
     * Get driving status items
     *
     * @param Request $request
     * @return array|null
     */
    public function getDrivingResource(Request $request)
    {
        $driving = null;

        try {
            $driving = $this->drivingService->getDriving($request->user->user_id, $request->userTime);
        } catch (\Exception $e) {
            return response()->json([
                'title' => 'Unable to Get Driving Information',
                'message' => 'We are unable to get your driving information at this time. Please try again, or contact your CSA if you need further assistance.',
                'origin_message' => $e->getMessage(),
                'origin_status' => $e->getCode()
            ], 500);
        }

        return json_encode($driving);
    }

    public function submitEndingOdometer(Request $request)
    {
        try {
            $response = $this->drivingService->updateEndingOdometer($request->user->user_id, $request->get('ending_day'), $request->get('ending_odometer'), $request->userTime);
        } catch (\Exception $e) {
            return response()->json([
                'title' => 'Unable to Update Odometer',
                'message' => 'We were unable to update your ending odometer at this time. Please try again, or contact your CSA for further assistance.',
                'origin_message' => $e->getMessage(),
                'origin_status' => $e->getCode()
            ], 500);
        }

        if (isset($response['success']) && $response['success']) {
            return response()->json(['miroute_page' => $response['miroute_page'], 'drivingResource' => $response['drivingResource']], 200);
        } else {
            $statusCode = !isset($response['statusCode']) || empty($response['statusCode']) ? 500 : $response['statusCode'];
            return response()->json([
                'title' => $response['title'],
                'message' => $response['message'],
                'origin_message' => isset($response['origin_message']) ? $response['origin_message'] : $response['message'],
                'origin_status' => isset($response['origin_status']) ? $response['origin_status'] : $statusCode,
            ], $statusCode);
        }
    }

    public function submitStartingOdometer(Request $request)
    {
        try {
            $response = $this->drivingService->submitStartingOdometer($request->user->user_id, $request->get('starting_odometer'),
                $request->get('business_purpose'), $request->get('stop_id'), $request->get('stop_type'), $request->userTime);
        } catch (\Exception $e) {
            return response()->json([
                'title' => 'Unable to Update Odometer',
                'message' => 'We were unable to update your starting odometer at this time. Please try again, or contact your CSA for further assistance.',
                'origin_message' => $e->getMessage(),
                'origin_status' => $e->getCode()
            ], 500);
        }

        if (isset($response['success']) && $response['success']) {
            return response()->json(['message' => $response['message'], 'drivingResource' => $response['drivingResource']], 200);
        } else {
            $statusCode = !isset($response['statusCode']) || empty($response['statusCode']) ? 500 : $response['statusCode'];
            return response()->json([
                'title' => $response['title'],
                'message' => $response['message'],
                'origin_message' => isset($response['origin_message']) ? $response['origin_message'] : $response['message'],
                'origin_status' => isset($response['origin_status']) ? $response['origin_status'] : $statusCode,
            ], $statusCode);
        }
    }

    public function getNearbyStops(Request $request)
    {
        try {
            $response = $this->drivingService->getNearbyStops($request->user->user_id, $request->get('latitude'), $request->get('longitude'), $request->userTime);
        } catch (\Exception $e) {
            return response()->json([
                'title' => 'Unable to Get Nearby Stops',
                'message' => 'We are unable to get saved stops at your location right now. Please try again, or contact your CSA if you need further assistance.',
                'origin_message' => $e->getMessage(),
                'origin_status' => $e->getCode()
            ], 500);
        }

        if (isset($response['success']) && $response['success']) {
            return response()->json(['message' => $response['message'], 'stops' => $response['stops']], 200);
        } else {
            $statusCode = !isset($response['statusCode']) || empty($response['statusCode']) ? 500 : $response['statusCode'];
            return response()->json([
                'title' => $response['title'],
                'message' => $response['message'],
                'origin_message' => isset($response['origin_message']) ? $response['origin_message'] : $response['message'],
                'origin_status' => isset($response['origin_status']) ? $response['origin_status'] : $statusCode,
            ], $statusCode);
        }
    }

    public function submitStop(Request $request)
    {
        try {
            $response = $this->drivingService->submitStop($request->user->user_id, $request->get('stop_id'), $request->get('stop_type'), $request->get('business_purpose'), $request->userTime);
        } catch (\Exception $e) {
            return response()->json([
                'title' => 'Unable to Submit Your Stop',
                'message' => 'We were unable to submit your stop at this time. Please try again, or contact your CSA for further assistance.',
                'origin_message' => $e->getMessage(),
                'origin_status' => $e->getCode()
            ], 500);
        }

        if (isset($response['success']) && $response['success']) {
            return response()->json(['message' => $response['message'], 'drivingResource' => $response['drivingResource']], 200);
        } else {
            $statusCode = !isset($response['statusCode']) || empty($response['statusCode']) ? 500 : $response['statusCode'];
            return response()->json([
                'title' => $response['title'],
                'message' => $response['message'],
                'origin_message' => isset($response['origin_message']) ? $response['origin_message'] : $response['message'],
                'origin_status' => isset($response['origin_status']) ? $response['origin_status'] : $statusCode,
            ], $statusCode);
        }
    }

    public function submitNewStop(Request $request)
    {
        try {
            $response = $this->drivingService->submitNewStop($request->user->user_id, $request->get('stop'), $request->userTime);
        } catch (\Exception $e) {
            return response()->json([
                'title' => 'Unable to Create Your Stop',
                'message' => 'We were unable to create your stop at this time. Please try again, or contact your CSA for further assistance.',
                'origin_message' => $e->getMessage(),
                'origin_status' => $e->getCode()
            ], 500);
        }

        if (isset($response['success']) && $response['success']) {
            return response()->json(['message' => $response['message'], 'new_stop_id' => $response['new_stop_id']], 200);
        } else {
            $statusCode = !isset($response['statusCode']) || empty($response['statusCode']) ? 500 : $response['statusCode'];
            return response()->json([
                'title' => $response['title'],
                'message' => $response['message'],
                'origin_message' => isset($response['origin_message']) ? $response['origin_message'] : $response['message'],
                'origin_status' => isset($response['origin_status']) ? $response['origin_status'] : $statusCode,
            ], $statusCode);
        }
    }

    public function updateTripMileage(Request $request)
    {
        try {
            $response = $this->drivingService->updateTripMileage($request->user->user_id, $request->get('trip_id'), $request->get('business_purpose'), $request->get('mileage_adjusted'), $request->get('mileage_comment'), $request->userTime);
        } catch (\Exception $e) {
            return response()->json([
                'title'=>'Unable to Update Mileage',
                'message' => 'We were unable to update your trip mileage at this time. Please try again, or contact your CSA for further assistance.',
                'origin_message' => $e->getMessage(),
                'origin_status' => $e->getCode()
            ], 500);
        }

        if (isset($response['success']) && $response['success']) {
            return response()->json(['message' => $response['message'], 'drivingResource' => $response['drivingResource']], 201);
        } else {
            $statusCode = !isset($response['statusCode']) || empty($response['statusCode']) ? 500 : $response['statusCode'];
            return response()->json([
                'title' => $response['title'],
                'message' => $response['message'],
                'origin_message' => isset($response['origin_message']) ? $response['origin_message'] : $response['message'],
                'origin_status' => isset($response['origin_status']) ? $response['origin_status'] : $statusCode,
            ], $statusCode);
        }
    }

    public function getCommuteMileage(Request $request)
    {
        try {
            $response = (new CommuteServiceProvider(app()))->getCommuteMileage($request->user->user_id, $request->userTime);
        } catch (\Exception $e) {
            return response()->json([
                'title' => 'Unable to Get Commuter Mileage',
                'message' => 'We were unable to get commute mileage information at this time. Please try again, or contact your CSA if you need further assistance.',
                'origin_message' => $e->getMessage(),
                'origin_status' => $e->getCode()
            ], 500);
        }

        if (isset($response['success']) && $response['success']) {
            return response()->json(['message' => $response['message'],
                'commute_mileage' => $response['commute_mileage'], 'show_commute_mileage'=>$response['show_commute_mileage']], 201);
        } else {
            $statusCode = !isset($response['statusCode']) || empty($response['statusCode']) ? 500 : $response['statusCode'];
            return response()->json([
                'title' => $response['title'],
                'message' => $response['message'],
                'origin_message' => isset($response['origin_message']) ? $response['origin_message'] : $response['message'],
                'origin_status' => isset($response['origin_status']) ? $response['origin_status'] : $statusCode,
            ], $statusCode);
        }
    }
}
