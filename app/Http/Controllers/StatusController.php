<?php

namespace App\Http\Controllers;

use App\Company;
use App\Driver;
use App\Providers\CoordinatorServiceProvider;
use App\Providers\StatusServiceProvider;
use App\Providers\UserServiceProvider;
use App\User;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use Illuminate\Support\Facades\Auth;

class StatusController extends Controller
{
    private $statusServiceProvider;

    public function __construct()
    {
        $this->statusServiceProvider = new StatusServiceProvider(app());
    }

    public function updateCurrentPage(Request $request)
    {
        try {
            $response = $this->statusServiceProvider->updateCurrentPage($request->user->user_id, $request->get('action'), $request->get('page'), $request->userTime);
        } catch (\Exception $e) {
            return response()->json([
                'title' => 'Unable to Update Page',
                'message' => 'We are unable to update your current page at this time. Please try again, or contact your CSA for further assistance.',
                'origin_message' => $e->getMessage(),
                'origin_status' => $e->getCode()
            ], 500);
        }

        if (isset($response['success']) && $response['success']) {
            return response()->json(['message' => $response['message']], 200);
        } else {
            $statusCode = !isset($response['statusCode']) || empty($response['statusCode']) ? 500 : $response['statusCode'];
            return response()->json([
                'title' => $response['title'],
                'message' => $response['message'],
                'origin_message' => isset($response['origin_message']) ? $response['origin_message'] : $response['message'],
                'origin_status' => isset($response['origin_status']) ? $response['origin_status'] : $statusCode,
            ], $statusCode);
        }
    }
}
