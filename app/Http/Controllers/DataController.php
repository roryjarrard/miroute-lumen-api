<?php

namespace App\Http\Controllers;

use App\User;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use JWTAuth;

use Illuminate\Support\Facades\Mail;
use App\Mail\CodeEmail;

class DataController extends Controller
{
    public function sendEmail($username, $code) {
        Mail::to('roryjarrard@gmail.com')->send(new CodeEmail($username, $code));
    }

    public function open(Request $request)
    {
        $data = "This data is open and can be accessed without the client being authenticated";

        //$this->sendEmail('rjarrard', 754687);
        try {
            // make the call to the rules engine
            /*$client = new Client([
                'verify' => false,
                'base_uri' => $rules_engine_url
            ]);
            try {
                $response = $client->post('/commute-mileage/calculate', [
                    RequestOptions::JSON => $parameters
                ]);
            } catch (ClientException $e) {
                // todo log
                $param_json = json_encode($parameters);
                $errors_json = json_encode($e->getResponse()->getBody());
                $exception_message = "Request params: $param_json \n Response: $errors_json";
                throw new \Exception($exception_message);
            }*/

            // use the container name in the uri
            $client = new Client([
                'verify' => false,
                'base_uri' => 'https://rules-engine-nginx'
            ]);
            $response = $client->get('/test');
            $statusCode = $response->getStatusCode();
            $output = $response->getBody();
            $data .= $output;
        } catch (\Exception $e) {
            $data .= "\nthat failed " . $e->getMessage();
        }

        return response()->json(compact('data'),200);
    }

    public function closed(Request $request)
    {
        $user = $request->user;
        echo var_dump($user);
        $id = $user->user_id;
        $data = "Only authorized users can see this " . $id;
        return response()->json(compact('data'),200);
    }
}
