<?php

namespace App\Http\Middleware;

use Closure;

class MeasureExecutionTime
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // Get the response
        $response = $next($request);


        // Calculate execution time
        defined('LUMEN_START') or define('LUMEN_START', microtime(true)); // for phpUnit
        $executionTime = microtime(true) - LUMEN_START;

        // add time to response
        if (gettype($response->getContent()) == 'object') {
            $content = json_decode($response->getContent(), true) + ['execution_time' => $executionTime,];

            $response->setContent(json_encode($content));
        }

        // Return the response
        return $response;
    }
}
