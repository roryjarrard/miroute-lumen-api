<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Admin extends Model
{
    protected $table = 'ap_admin';

    protected $fillable = [
        'user_id', 'company_admin', 'company_approver', 'isLimitedAdmin'
    ];
}